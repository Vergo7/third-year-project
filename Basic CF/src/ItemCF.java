import au.com.bytecode.opencsv.CSVReader;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;

import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class ItemCF {

    static double[][] similarityMatrix;
    static int itemSize;
    static int userSize;
    static Instances dataset;

    public ItemCF() throws Exception {
        CSVtoARFF("u1base");
        DataSource source = new DataSource("u1base.arff");
        dataset = source.getDataSet();
        itemSize = dataset.numAttributes();
        userSize = dataset.numInstances();
    }

    public static void main(String args[]) throws Exception {
        CSVtoARFF("u1test");
        ItemCF object = new ItemCF();
        DataSource testSource = new DataSource("u1test.arff");
        Instances userRating = testSource.getDataSet();

        for(int recommendationSize = 0; recommendationSize < (userSize - 1); recommendationSize++) {

            Instance testData = userRating.instance(recommendationSize);
            Instance userData = userRating.instance(recommendationSize);

            List<Integer> unratedItems = new ArrayList<Integer>();  // list of items that have been rated by user in test file
            List<Integer> ratedItems = new ArrayList<Integer>();    // list of items that have been rated by user in base file

            for (int i = 0; i < itemSize; i++) {
                if (testData.value(i) != 0.0) {
                    unratedItems.add(i);    // add item to list from test data if it has been rated (for comparison)
                }

                if (userData.value(i) != 0.0) {
                    ratedItems.add(i);  // add item to list from base data if it has been rated as this is what we'll use to approximate unknown score
                }
            }
            List<RecommendationRecord> finalRanks = new ArrayList<RecommendationRecord>();

            for (int unratedItem_index : unratedItems) {

                String unratedItem_name = testData.attribute(unratedItem_index).name();
                double unratedItem_score = object.predictedRating(testData, unratedItem_index, ratedItems);

                if (Double.isNaN(unratedItem_score) == false) {  // check in case there are no users at all who have rated both items
                    RecommendationRecord rec = new RecommendationRecord();
                    rec.attributeName = unratedItem_name;
                    rec.score = unratedItem_score;
                    rec.item_index = unratedItem_index;

                    finalRanks.add(rec);
                }
            }

            Collections.sort(finalRanks);

            System.out.println("For user " + recommendationSize + " with " + finalRanks.size() + " test rankings");
/*
            for(int i = 0; i < finalRanks.size(); i++) {
                System.out.println(finalRanks.get(i) + " Actual rating " + testData.value(finalRanks.get(i).item_index));
            }*/

            double actualRating;
            double predictedRating;
            double MAE = 0.0;

            for(int i = 0; i < finalRanks.size(); i++) {
                predictedRating = finalRanks.get(i).score;
                actualRating = testData.value(finalRanks.get(i).item_index);

                MAE = MAE + Math.abs((predictedRating - actualRating));
            }

            MAE = (MAE)/(finalRanks.size());

            System.out.println("For user " + recommendationSize + " mean absolute error is " + MAE);
        }
    }

    public static void makeMatrix() {
    similarityMatrix = new double[itemSize][itemSize];
        for(int i = 0; i < itemSize; i++) {
            for(int j = 0; j < itemSize; j++) {
                similarityMatrix[i][j] = itemSimilarity(i, j);
            }
        }
    }

    public static double predictedRating(Instance user, int item_index, List<Integer> ratedItems) {
        double numerator = 0.0;
        double denominator = 0.0;
        double similarity;
        double ratedItem_rating;
        double finalRating;

        for(int ratedItem_index : ratedItems) {
            similarity = itemSimilarity(item_index, ratedItem_index);
            ratedItem_rating = normalize(user.value(ratedItem_index));

            numerator = numerator + (similarity * ratedItem_rating);
            denominator = denominator + Math.abs(similarity);
        }

        finalRating = numerator/denominator;
        return denormalize(finalRating);
    }

    public static double normalize(double rating) {
        double min_rating = 1.0;
        double max_rating = 5.0;
        double normalized_rating;

        normalized_rating = (2*(rating - min_rating) - (max_rating- min_rating))/(max_rating - min_rating);

        return normalized_rating;
    }

    public static double denormalize(double rating) {
        double min_rating = 1.0;
        double max_rating = 5.0;
        double denormalized_rating;

        denormalized_rating = ((rating + 1) * (max_rating - min_rating))/2 + min_rating;

        return denormalized_rating;
    }

    public static double itemSimilarity(int item1_index, int item2_index) {
        List<Instance> commonUsers = commonUsers(item1_index, item2_index); // list of users who have rated both these items
        double numerator = 0.0;
        double denominator1_noRoot = 0.0;
        double denominator2_noRoot = 0.0;
        double denominator1_root;
        double denominator2_root;
        double item1_rating;
        double item2_rating;

        // refer to pg. 17 of "http://guidetodatamining.com/guide/ch3/DataMining-ch3.pdf" for algorithm

        for(Instance user : commonUsers) {
            item1_rating = user.value(item1_index);
            item2_rating = user.value(item2_index);

            numerator = numerator + ((item1_rating - average(user)) * (item2_rating - average(user)));
            denominator1_noRoot = denominator1_noRoot + Math.pow((item1_rating - average(user)),2);
            denominator2_noRoot = denominator2_noRoot + Math.pow((item2_rating - average(user)),2);
        }

        denominator1_root = Math.sqrt(denominator1_noRoot);
        denominator2_root = Math.sqrt(denominator2_noRoot);

        return ((numerator)/(denominator1_root * denominator2_root));
    }

    public static List<Instance> commonUsers(int item1_index, int item2_index) {

        List<Instance> commonUsers = new LinkedList<Instance>();
        Instance currentUser;
        double item1_value;
        double item2_value;

        for(int i = 0; i < userSize; i++) {
            currentUser = dataset.instance(i);
            item1_value = currentUser.value(item1_index);
            item2_value = currentUser.value(item2_index);
            if((item1_value > 0.0) & (item2_value > 0.0)) {
                commonUsers.add(currentUser);   // add to list if current user has rated both item 1 and 2
            }
        }

        return commonUsers;
    }

    public static double average(Instance user) {
        double itemsRated = 0.0;
        double sum = 0.0;
        double item_rating;

        for(int i = 0; i < user.numAttributes(); i++) {
            item_rating = user.value(i);
            if(item_rating > 0.0) {
                itemsRated = itemsRated + 1;
                sum = sum + item_rating;
            }
        }

        return (sum/itemsRated);
    }

    public static void CSVtoARFF(String csvFilename) throws Exception {
        FastVector atts = new FastVector();
        Instances data;

        for(int i = 0; i < 1682; i++) {
            atts.addElement(new Attribute("item_" + (i+1)));
        }

        data = new Instances("MyRelation", atts, 0);

        CSVReader reader = new CSVReader(new FileReader(csvFilename + ".csv"));

        int userIndex = 1;
        double item_id;
        double rating;
        double[] vals = new double[data.numAttributes()];

        String [] nextLine;
        while ((nextLine = reader.readNext()) != null) {
            // nextLine[] is an array of values from the line

            if(Double.parseDouble(nextLine[0]) == userIndex) {
                item_id = Double.parseDouble(nextLine[1]);
                rating = Double.parseDouble(nextLine[2]);

                vals[(int)item_id - 1] = rating;
            } else {
                data.add(new Instance(1.0, vals));
                userIndex++;
                vals = new double[data.numAttributes()];
                item_id = Double.parseDouble(nextLine[1]);
                rating = Double.parseDouble(nextLine[2]);

                vals[(int)item_id - 1] = rating;
            }
        }

        BufferedWriter writer = new BufferedWriter(new FileWriter(csvFilename + ".arff"));
        writer.write(data.toString());
        writer.flush();
        writer.close();
    }
}

