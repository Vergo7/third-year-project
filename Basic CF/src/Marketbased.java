import edu.uci.ics.jung.graph.DirectedSparseMultigraph;
import edu.uci.ics.jung.graph.Graph;
import org.apache.commons.math3.distribution.BinomialDistribution;
import org.apache.commons.math3.distribution.EnumeratedDistribution;
import org.apache.commons.math3.exception.MathArithmeticException;
import org.apache.commons.math3.util.Pair;
import weka.core.EuclideanDistance;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;
import weka.core.neighboursearch.LinearNNSearch;

import java.util.*;

/**
 * Created by Varun on 12/11/2014.
 */

/*
Things to do and clarify -
 Still need to incorporate the simulation cycle probability thingy
 Need to get number of items picked to rate by user modelled as the binomial distribution thing
 Need to figure out how to actually calculate Mean Average Error - currently for users who rate items we just assign the rating as what they rated in the movielens matrix. What is this supposed to be compared with?

*/

/*
Important points to note :

item IDs for the objects are ONE-INDEXED
movielens item IDs in the matrix are ONE-INDEXED as well
user IDs stored in the graph are ZERO-INDEXED
the movielens user IDs in the matrix are ONE-INDEXED

 */


public class Marketbased {

    static HashMap<Integer, graphNode> IDNodeMap;   // hashmap to access user nodes in the graph by user ID
    static HashMap<Integer, item> IDItemMap;    // hashmap to access items by item ID

    static double deltaP;
    static double deltaR;
    static int numberOfNeighbours;
    static double maxBidValue;
    static int maxCyclesSkip;

    static void init(double deltaP, double deltaR, int numberOfNeighbours, double maxBidValue, int maxCyclesSkip) {
        Marketbased.deltaP = deltaP;
        Marketbased.deltaR = deltaR;
        Marketbased.numberOfNeighbours = numberOfNeighbours;
        Marketbased.maxBidValue = maxBidValue;
        Marketbased.maxCyclesSkip = maxCyclesSkip;
    }

    public static void main(String[] args) throws Exception {
        execute();
    }

    public static double execute() throws Exception {

        Instances MovieLensRatingsMatrix = null;
        try {
            MovieLensRatingsMatrix = ConverterUtils.DataSource.read("fullRatingsMatrix.arff");
        } catch (Exception e) {
            e.printStackTrace();
        }

        Instances ratingsMatrix = null;
        try {
            ratingsMatrix = ConverterUtils.DataSource.read("u1base.arff");
        } catch (Exception e) {
            e.printStackTrace();
        }

        Instances sorted_data = null;
        try {
            sorted_data = ConverterUtils.DataSource.read("u_sorted.csv");

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        Instances sorted_dataByUser = null;
        try {
            sorted_dataByUser = ConverterUtils.DataSource.read("u_sortedByUser.csv");

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        Instances testData = null;
        try {
            testData = ConverterUtils.DataSource.read("u1test.csv");
        } catch (Exception e) {
            e.printStackTrace();
        }

        List<item> surrogateItems = itemSurrogates(sorted_data);   // gets the item ID and number of ratings for items that have more than 350 ratings

        ArrayList<Integer> surrogateItemIDs = new ArrayList<Integer>();
        for(item newItem : surrogateItems) {
            surrogateItemIDs.add(newItem.getItemID());
        }

        double leastBid = maxBidValue;  // initialise to maximum possible bid value
        int leastIndex = 0; // stores index of the item to which least bid was assigned

        for(int i = 0; i < surrogateItems.size(); i++) {
            item currentItem = surrogateItems.get(i);
            currentItem.setItemBid(randomBid(maxBidValue));  // assign random bid to item

            // we do this to find the least bid assigned to an item, and also store its index
            if(currentItem.getItemBid() < leastBid) {
                leastBid = currentItem.getItemBid();
                leastIndex = i;
            }
        }

        double budget = leastBid * surrogateItems.get(leastIndex).getNumberOfRatings(); // set budget according to simulation in paper

        // once budget has been decided on we go through all the surrogate items and set each one's budget to be the same fixed value
        for(int i = 0; i < surrogateItems.size(); i++) {
            item currentItem = surrogateItems.get(i);
            currentItem.setBudget(budget);
        }

        removeRatings(surrogateItems, ratingsMatrix);   // sets all ratings for the new items to be 0
        Graph<graphNode, graphEdge> influenceGraph = createGraph(ratingsMatrix, surrogateItems);    // creates the graph while ignoring ratings for the new items

        int cycle = 0;
        int cyclesSkipped = 0;

        ArrayList<Integer> itemsSeeded = new ArrayList<Integer>();
        HashMap<Integer, Integer> userRatingsPerDay = userRatingsPerDayMap(sorted_dataByUser);

        while (surrogateItems.isEmpty() == false) {
            if(cyclesSkipped > maxCyclesSkip) {
                break;
            }

            if(simCycleOccurs(cycle) == false) {
                cycle++;
                cyclesSkipped++;
                continue;
            } else {
                cycle++;
            }

            Collections.sort(surrogateItems, new Comparator<item>() {    // sort the items in the list based on their bid and rate of uptake product
                @Override
                public int compare(item i1, item i2) {
                    return Double.compare(i2.getBidUptakeProduct(), i1.getBidUptakeProduct()); // sorts in descending order
                }
            });

            // get seed set of users for each new item
            for (int i = 0; i < surrogateItems.size(); i++) {
                if (surrogateItems.get(i).getBudget() > 0) {
                    int seedSetSize = (int) ((surrogateItems.get(i).getBudget()) / (surrogateItems.get(i).getItemBid()));
                    List<graphNode> seeds = selectSeeds(influenceGraph, seedSetSize, ratingsMatrix, MovieLensRatingsMatrix, surrogateItems.get(i).getItemID());

                    // sum of the discounted degrees of the users in the seed set, used as a measure of influence
                    double seedInfluence = 0;

                    for(graphNode currentSeed : seeds) {
                        // don't add an item to a user's EPR list if that item has already been added before
                        if(currentSeed.EPRList.contains(surrogateItems.get(i)) == false) {
                            currentSeed.EPRList.add(surrogateItems.get(i));
                            currentSeed.sortEPR();  // once new item has been added we must call sort to determine its position on the EPR list
                        }
                        if(surrogateItems.get(i).usersList.contains(currentSeed) == false) {
                            // this user has just had the item added to his EPR list, so we append this user to the usersList for the item
                            // add this user only if he hasn't been added before
                            surrogateItems.get(i).usersList.add(currentSeed);
                        }
                        seedInfluence += currentSeed.getDiscountedDegree();
                    }

                    List<graphNode> optimalSeeds = selectOptimalSeeds(influenceGraph, seedSetSize, ratingsMatrix, MovieLensRatingsMatrix, surrogateItems.get(i).getItemID());

                    // sum of the discounted degrees of the users in the optimal seed set, used as a measure of influence
                    double optimalSeedInfluence = 0;

                    for(graphNode currentOptimalSeed : optimalSeeds) {
                        optimalSeedInfluence += currentOptimalSeed.getDiscountedDegree();
                    }

                    surrogateItems.get(i).seeds = seeds; // set the list of seeds for item in item object to be the list we just obtained above

                    // calculates the price paid to the users who rate the item
                    surrogateItems.get(i).setPayment((seedInfluence / optimalSeedInfluence) * surrogateItems.get(i).getItemBid());
                }
            }

            for (graphNode currentUser : influenceGraph.getVertices()) {
                List<item> itemsToRate = selectItemsToRate(currentUser, userRatingsPerDay);    // gets the new items that the user has picked from their EPR list to rate

                for(item currentItem : itemsToRate) {

                    double itemRating = MovieLensRatingsMatrix.instance(currentUser.getID()).value(currentItem.getItemID() - 1); // user ID is 0-indexed while item ID is 1-indexed
                    ratingsMatrix.instance(currentUser.getID()).setValue(currentItem.getItemID() - 1, itemRating);  // sets rating for new item in ratings matrix to be the same as the one in the full movielens one

                    // System.out.println("User " + (currentUser.getID()+1) + " seeded item " + currentItem.getItemID() + " = " + itemRating) ;
                    if(itemsSeeded.contains(currentItem.getItemID()) == false) {
                        itemsSeeded.add(currentItem.getItemID());
                    }
                    // since user has actually rated the item we add it to the corresponding list
                    currentItem.ratedUsersList.add(currentUser);

                    // subtract price paid to users who rate item from the overall budget
                    currentItem.setBudget(currentItem.getBudget() - currentItem.getPayment());
                }
            }

            for (int i = 0; i < surrogateItems.size(); i++) {
                item currentItem = surrogateItems.get(i);
                // if the budget is lesser than the bid or all users who have the item in their list have rated it, remove item from the list of new ones
                if ((currentItem.getBudget() < currentItem.getItemBid()) || (currentItem.usersList.containsAll(currentItem.ratedUsersList))) {
                    surrogateItems.remove(i);
                }

                currentItem.setRateOfUptake(rateOfUptake(currentItem));
            }
        }


        for(int i = 0; i < itemsSeeded.size(); i++) {
            // System.out.println("Item " + itemsSeeded.get(i) + " was seeded");
        }

        double meanAverageError = 0;
        double numberOfPredictions = 0;
        HashMap<Integer, double[]> newItemSimilarityMatrix = createNewItemSimilarityMatrix(surrogateItemIDs, ratingsMatrix);

        for(int i = 0; i < testData.numInstances(); i++) {
            Instance currentInstance = testData.instance(i);
            if(surrogateItemIDs.contains((int) currentInstance.value(1))) {
                int user_ID = (int) currentInstance.value(0);
                int item_ID = (int) currentInstance.value(1);

                double rating = currentInstance.value(2);
                double predictedRating = predictedRating(user_ID, item_ID, ratingsMatrix, newItemSimilarityMatrix);

                meanAverageError += Math.abs(predictedRating - rating);
                numberOfPredictions++;

                // System.out.println("User " + user_ID + " rated item " + item_ID + " = " + rating + " and it was predicted as = " + predictedRating);
            }
        }
        // System.out.println("Mean average error is " + (meanAverageError/numberOfPredictions));
        System.out.print(", " + (meanAverageError/numberOfPredictions));
        return (meanAverageError/numberOfPredictions);
    }

    static double randomBid(double maxBidValue) {
        return 0.0 + Math.random() * (maxBidValue - 0.0);
    }

    // method gets the item ID and number of ratings for items that have more than 350 ratings
    static List<item> itemSurrogates(Instances datasetCSV) {
        ArrayList<item> surrogateItemIDs = new ArrayList<item>(); // list that stores the IDs of items that have more than 350 ratings
        IDItemMap = new HashMap<Integer, item>();

        int ID = 1;
        int total = 0;  // variable to store the number of ratings for a particular item

        for(int i = 0; i < datasetCSV.numInstances(); i++) {
            if(ID == datasetCSV.instance(i).value(1)) {
                total++;
                // if it is the last user entry we're looking at we must definitely execute the below code
                if(i != datasetCSV.numInstances()-1) {
                    continue;   // this ensures that we don't get to bottom part till counting has been completed
                }
            }

            if(total > 350) {
                item currentItem = new item(ID, total);
                putItem(ID, currentItem);   // puts item in hashmap so that we can retrieve it easily later
                surrogateItemIDs.add(currentItem);
            }

            total = 1;  // breaking out of initial if statement means that we have moved on to new ID, so need to set total as 1 to count the current new item
            ID++;
        }

        return surrogateItemIDs;
    }

    // this method sets all ratings of surrogate item columns to be 0
    static void removeRatings(List<item> surrogateItems, Instances ratingsMatrix) throws Exception {
        // go through all instances in the ratings matrix
        for(int i = 0; i < ratingsMatrix.numInstances(); i++) {
            Instance currentItem = ratingsMatrix.instance(i);
            // go through all the new item columns and set the ratings to 0 for these
            for(item newItem : surrogateItems) {
                currentItem.setValue(newItem.getItemID()-1, 0); // attribute field is 0-indexed while item ID is 1-indexed
            }
        }
    }

    // this method returns the initial influence graph
    static Graph<graphNode, graphEdge> createGraph(Instances ratingsMatrix, List<item> surrogateItems) {
        Graph<graphNode, graphEdge> influenceGraph = new DirectedSparseMultigraph<graphNode, graphEdge>();

        IDNodeMap = new HashMap<Integer, graphNode>();

        for(int i = 0; i < ratingsMatrix.numInstances(); i++) {
            graphNode node = new graphNode(i);
            influenceGraph.addVertex(node);
            IDNodeMap.put(i, node);
        }

        LinearNNSearch kNN = new LinearNNSearch(ratingsMatrix);

        try {
            EuclideanDistance df = new EuclideanDistance(ratingsMatrix);
            String range = "";

            ArrayList<Integer> surrogateItemIDs = new ArrayList<Integer>();
            for(item newItem : surrogateItems) {
                surrogateItemIDs.add(newItem.getItemID());
            }

            // we don't do numAttributes + 1 since we want to leave out the user ID attribute for distance computation
            for(int j = 1; j < ratingsMatrix.numAttributes(); j++) {
                // we don't want to include ratings of the new items when making the graph for distance computation
                if(surrogateItemIDs.contains(j) == false) {
                    if (j == 1) {
                        range = range + "1";
                    } else {
                        range = range + "," + j;
                    }
                }
            }
            df.setAttributeIndices(range);   // 1-indexed, excluding the ID field from the computation of nearest neighbour distance
            kNN.setDistanceFunction(df);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Instances neighbours = null;
        double[] distances = null;

        for(int i = 0; i < ratingsMatrix.numInstances(); i++) {
            try {
                neighbours = kNN.kNearestNeighbours(ratingsMatrix.instance(i), numberOfNeighbours);  // get the nearest neighbours of the current instance
                distances = kNN.getDistances(); // get the distances from these 5 nearest neighbours
            }  catch (Exception e) {
                e.printStackTrace();
            }

            double[] similarities = new double[distances.length];
            for (int j = 0; j < distances.length; j++) {
                similarities[j] = 1.0 / distances[j];   // distance is inversely proportional to how similar 2 instances are, hence take the inverse
            }
            for(int j = 0; j < neighbours.numInstances(); j++) {
                Instance currNeighbor = neighbours.instance(j);
                int user = (int) currNeighbor.value(currNeighbor.numAttributes()-1)-1;    // get the user ID (0-indexed) of the neighbour from the ID attribute field
                double similarity = similarities[j];
                graphEdge Edge = new graphEdge(similarity);
                graphNode Node = getNode(i);
                graphNode neighbourNode = getNode(user);

                influenceGraph.addEdge(Edge, Node, neighbourNode);  // add edge between the node and the neighbour found with similarity as the weight
                Node.setDegree(Node.getDegree() + 1);   // whenever we make a new outgoing edge we increment the degree of the node by 1
            }
        }
        return influenceGraph;
    }

    static graphNode getNode(int userID) {
        return IDNodeMap.get(userID);
    }
    static item getItem(int itemID) {return IDItemMap.get(itemID);}
    static void putItem(int itemID, item currentItem) {IDItemMap.put(itemID, currentItem);}

    static List<graphNode> selectSeeds(Graph<graphNode, graphEdge> influenceGraph, int size, Instances ratingsMatrix, Instances MovieLensRatingsMatrix, int itemID) {

        List<graphNode> vertices = new ArrayList<graphNode>(influenceGraph.getVertices());  // get a list of vertices in the graph

        // this for loop iterates through the entire graph and discounts degree for each vertex based on whether the adjoining outgoing vertices are already in the item's seed set
        for(int i = 0; i < vertices.size(); i++) {
            int degree = vertices.get(i).getDegree();   // this is the original degree of the graph without any discounts
            graphNode currentNode = vertices.get(i);
            List<graphNode> outgoingNeighbours = new ArrayList<graphNode>(influenceGraph.getSuccessors(currentNode));   // get the vertices which are connected to the current vertex by an outgoing edge

            for(int j = 0; j < outgoingNeighbours.size(); j++) {
                if(getItem(itemID).seeds.contains(outgoingNeighbours.get(j))) { // if the neighbour is in the seed set for the item, discount the degree of current vertex by 1
                    degree = degree - 1;
                }
            }

            currentNode.setDiscountedDegree(degree);

            // this entire tempEPRlist section below is to figure out what place the item would occupy on the user's EPR list
            ArrayList<item> tempEPRlist = new ArrayList<item>(currentNode.EPRList);
            tempEPRlist.add(getItem(itemID));

            Collections.sort(tempEPRlist, new Comparator<item>() {    // sort the items in the list based on their bid and rate of uptake product
                @Override
                public int compare(item i1, item i2) {
                    return Double.compare(i2.getBidUptakeProduct(), i1.getBidUptakeProduct()); // sorts in descending order
                }
            });

            currentNode.setPu(itemSelectionProbability(tempEPRlist.indexOf(getItem(itemID)),tempEPRlist.size()));   // uptake probability is a function of bid and rate of uptake
        }

        Collections.sort(vertices, new Comparator<graphNode>() {    // sort the vertices in the list based on the prodct of their discountedDegree and uptake probability
            @Override
            public int compare(graphNode g1, graphNode g2) {
                return Double.compare(g2.getDiscountedDegree()*g2.getPu(), g1.getDiscountedDegree()*g1.getPu()); // sorts in descending order
            }
        });

        int numberOfSeeds = 0;  // variable to store current length of seeds list
        List<graphNode> seeds = new ArrayList<graphNode>();

        for(graphNode currentNode : vertices) {
            if(numberOfSeeds == size) {
                break;  // if the number of seeds in the list is equal to the size we specified, break the loop
            }

            // if the user rated the corresponding item in the movielens dataset but not in the training one, add it to the seeds
            if((MovieLensRatingsMatrix.instance(currentNode.getID()).value(itemID-1) > 0) && (ratingsMatrix.instance(currentNode.getID())).value(itemID-1) == 0) {
                if(getItem(itemID).ratedUsersList.contains(currentNode) == false) { // add to seeds only if the user hasn't already rated this item in a previous cycle
                    seeds.add(currentNode);
                    numberOfSeeds++;
                }
            }
        }

        return seeds;
    }

    // this method is exactly the same as above, but here we assume that the EPR Lists of users are empty, which means that for each user the item occupies first rank
    static List<graphNode> selectOptimalSeeds(Graph<graphNode, graphEdge> influenceGraph, int size, Instances ratingsMatrix, Instances MovieLensRatingsMatrix, int itemID) {

        List<graphNode> vertices = new ArrayList<graphNode>(influenceGraph.getVertices());  // get a list of vertices in the graph

        // this for loop iterates through the entire graph and discounts degree for each vertex based on whether the adjoining outgoing vertices are already in the item's seed set
        for(int i = 0; i < vertices.size(); i++) {
            int degree = vertices.get(i).getDegree();   // this is the original degree of the graph without any discounts
            graphNode currentNode = vertices.get(i);
            List<graphNode> outgoingNeighbours = new ArrayList<graphNode>(influenceGraph.getSuccessors(currentNode));   // get the vertices which are connected to the current vertex by an outgoing edge

            for(int j = 0; j < outgoingNeighbours.size(); j++) {
                if(getItem(itemID).seeds.contains(outgoingNeighbours.get(j))) { // if the neighbour is in the seed set for the item, discount the degree of current vertex by 1
                    degree = degree - 1;
                }
            }

            currentNode.setDiscountedDegree(degree);
        }

        Collections.sort(vertices, new Comparator<graphNode>() {    // sort the vertices in the list based on their discountedDegree
            @Override
            public int compare(graphNode g1, graphNode g2) {
                return Integer.compare(g2.getDiscountedDegree(), g1.getDiscountedDegree()); // sorts in descending order
            }
        });

        int numberOfSeeds = 0;  // variable to store current length of seeds list
        List<graphNode> seeds = new ArrayList<graphNode>();

        for(graphNode currentNode : vertices) {
            if(numberOfSeeds == size) {
                break;  // if the number of seeds in the list is equal to the size we specified, break the loop
            }

            // if the user rated the corresponding item in the movielens dataset but not in the training one, add it to the seeds
            if((MovieLensRatingsMatrix.instance(currentNode.getID()).value(itemID-1) > 0) && (ratingsMatrix.instance(currentNode.getID())).value(itemID-1) == 0) {
                if(getItem(itemID).ratedUsersList.contains(currentNode) == false) { // add to seeds only if the user hasn't already rated this item in a previous cycle
                    seeds.add(currentNode);
                    numberOfSeeds++;
                }
            }
        }

        return seeds;
    }

    // probability of user selecting item with specified rank from his EPR list as described in the paper
    static double itemSelectionProbability(int rank, int size) {
        double alpha;
        double alphaInverse = 0.0;

        for(int i = 1; i <= size; i++) {
            alphaInverse = alphaInverse + deltaP*(Math.exp(-deltaP*i));
        }

        alpha = (1/alphaInverse);

        return alpha*deltaP*Math.exp(-deltaP*rank);
    }

    static double rateOfUptake(item currentItem) {
        double numerator = 0.0;
        double denominator = 0.0;

        for(int i = 0; i < currentItem.ratedUsersList.size(); i++) {
            graphNode currentUser = currentItem.ratedUsersList.get(i);
            int itemRank = currentUser.EPRList.indexOf(currentItem); // rank is 0-indexed
            numerator = numerator + itemSelectionProbability(itemRank, currentUser.EPRList.size());
        }

        for(int i = 0; i < currentItem.usersList.size(); i++) {
            graphNode currentUser = currentItem.usersList.get(i);
            int itemRank = currentUser.EPRList.indexOf(currentItem);
            denominator = denominator + itemSelectionProbability(itemRank, currentUser.EPRList.size());
        }

        return (numerator/denominator);
    }

    // creates map that stores the number of ratings made by each user
    static HashMap<Integer, Integer> userRatingsPerDayMap(Instances sorted_dataByUser) {
        HashMap<Integer, Integer> userRatingsPerDay = new HashMap<Integer, Integer>();

        int userID = 1;
        int total = 0;

        // go through all the sorted users
        for(int i = 0; i < sorted_dataByUser.numInstances(); i++) {
            // if the user ID is equal to the user ID of the current instance
            if(userID == sorted_dataByUser.instance(i).value(0)) {
                // increment the total
                total++;
                // we skip the below steps only if it is not the last instance
                if(i != sorted_dataByUser.numInstances()-1) {
                    continue;
                }
            }

            // System.out.println("User " + userID + " has " + total + " ratings");

            // put the number of ratings by the user in the map
            userRatingsPerDay.put(userID, total);

            // instantiate the total of the next user we are going to consider as 1
            total = 1;
            userID++;
        }

        return userRatingsPerDay;
    }

    static List<item> selectItemsToRate(graphNode currentUser, HashMap<Integer, Integer> userRatingsPerDay) {
        int userID = currentUser.getID()+1; // user ID in the graph is 0-indexed while the ID in the movielens matrix is 1-indexed
        double numberOfRatings = userRatingsPerDay.get(userID);

        // there are 216 days between 19th September 1997 and 4th April 1998, the duration in which movielens ratings were taken
        double itemSelectionProbability = numberOfRatings/216;
        // if the number of ratings is more than the number of days just set the probability to max. (1)
        if(itemSelectionProbability > 1) {
            itemSelectionProbability = 1;
        }
        int k = 10;  // maximum number of items that can be selected by the user at once

        BinomialDistribution binomialDistribution = new BinomialDistribution(k, itemSelectionProbability);  // creates new binomial dist.

        int m = binomialDistribution.sample();

        List<item> itemsToRate = new ArrayList<item>();
        ArrayList<Pair<item, Double>> probabilitiyItemList = new ArrayList<Pair<item, Double>>();   // list to store each item and its corresponding probability of selection

        // go through user's EPR list and add each item and its corresponding probability in the list we defined above
        for(int i = 0; i < currentUser.EPRList.size(); i++) {
            item currentItem = currentUser.EPRList.get(i);
            double itemProbability = itemSelectionProbability(i, currentUser.EPRList.size()); // rank is 0-indexed
            probabilitiyItemList.add(new Pair<item, Double>(currentItem, itemProbability));
        }

        try {
            // Apache Maths library function to randomly select item from list, "http://stackoverflow.com/questions/9330394/how-to-pick-an-item-by-its-probability/24676724#24676724"
            EnumeratedDistribution<item> itemSelectionDistribution = new EnumeratedDistribution<item>(probabilitiyItemList);

            for (int j = 0; j < m; j++) {
                item selectedItem = itemSelectionDistribution.sample(); // randomly selects item from list according to its probability
                // if the item has already been picked for selection then skip it
                if (itemsToRate.contains(selectedItem) == false) {
                    itemsToRate.add(selectedItem);
                }
            }
        }  catch (MathArithmeticException e) {
            // thrown when user's EPR list is empty, don't need to do anything special
        }

        // System.out.println("User " + userID + " selected " + itemsToRate.size() + " items to rate from his EPR list");
        return itemsToRate;
    }

    static boolean simCycleOccurs(int cycle) {
        double betaInverse = 0.0;

        for(int i = 1; i <= cycle; i++) {
            betaInverse = betaInverse + deltaR*(Math.exp(-deltaR*i));
        }

        double beta;    // this normalisation constant uses the same formula that alpha does, except that in this case list size is replaced by cycle size
        beta = (1/betaInverse);

        double probability = beta*deltaR*Math.exp(-deltaR*cycle);

        if(probability > Math.random()) {
            return true;
        }

        return false;
    }

    // note that the user ID here is NOT the user ID of the graphs, but rather the MovieLens userID which is 1-indexed. HANDLE WITH CARE!!!!
    public static double predictedRating(int userID, int itemID, Instances ratingsMatrix, HashMap<Integer, double[]> newItemSimilarityMatrix) {
        double numerator = 0.0;
        double denominator = 0.0;
        double similarity;
        double ratedItem_rating;
        double finalRating;

        Instance user = ratingsMatrix.instance(userID-1); // as noted above, this user ID is 1-indexed while instances field is 0-indexed

        // we skip the final attribute since it's the user ID
        for(int i = 0; i < ratingsMatrix.numAttributes()-1; i++) {

            if((itemID-1) == i) {
                continue;   // don't want to include the item itself when approximating score
            }

            int item2_ID = i+1; // the ID of the second item is one more than its attribute index

            similarity = newItemSimilarityMatrix.get(itemID)[item2_ID-1]; // gets row of similarities to other items from hashmap by item ID, after that Item IDs 1-indexed and array row fields 0-indexed

            // if similarity between the two items exists and the user has rated this other item
            // similarity threshold seems to greatly reduce the MAE
            if((Double.isNaN(similarity) == false) && (user.value(item2_ID - 1) > 0)  && (similarity > 0)) {

                ratedItem_rating = normalize(user.value(item2_ID - 1));
                numerator = numerator + (similarity * ratedItem_rating);
                denominator = denominator + Math.abs(similarity);
            }
        }


        if((numerator > 0 == false) && (denominator > 0 == false)) {
            // System.out.println("Used average rating");
            return average(user);
        }

        finalRating = numerator/denominator;
        return denormalize(finalRating);
    }

    public static double normalize(double rating) {
        double min_rating = 1.0;
        double max_rating = 5.0;
        double normalized_rating;

        normalized_rating = (2*(rating - min_rating) - (max_rating- min_rating))/(max_rating - min_rating);

        return normalized_rating;
    }

    public static double denormalize(double rating) {
        double min_rating = 1.0;
        double max_rating = 5.0;
        double denormalized_rating;

        denormalized_rating = ((rating + 1) * (max_rating - min_rating))/2 + min_rating;

        return denormalized_rating;
    }

    public static HashMap<Integer, double[]> createNewItemSimilarityMatrix(ArrayList<Integer> surrogateItemIDs, Instances ratingsMatrix) {
        HashMap<Integer, double[]> newItemSimilarityMatrix = new HashMap<Integer, double[]>();
        double[] similarityRow;

        for(int i = 0; i < surrogateItemIDs.size(); i++) {
            int newItemID = surrogateItemIDs.get(i);
            similarityRow = new double[ratingsMatrix.numAttributes()-1]; // attributes - 1 because we don't want to include user ID
            for(int j = 0; j < similarityRow.length; j++) {
                similarityRow[j] = itemSimilarity(newItemID, j+1, ratingsMatrix); // item ID is 1-indexed while attribute numbers are 0-indexed
            }

            newItemSimilarityMatrix.put(newItemID, similarityRow);
        }

        return newItemSimilarityMatrix;
    }

    public static double itemSimilarity(int item1_ID, int item2_ID, Instances ratingsMatrix) {
        List<Instance> commonUsers = commonUsers(item1_ID, item2_ID, ratingsMatrix); // list of users who have rated both these items
        double numerator = 0.0;
        double denominator1_noRoot = 0.0;
        double denominator2_noRoot = 0.0;
        double denominator1_root;
        double denominator2_root;
        double item1_rating;
        double item2_rating;

        // refer to pg. 17 of "http://guidetodatamining.com/guide/ch3/DataMining-ch3.pdf" for algorithm

        for(Instance user : commonUsers) {
            item1_rating = user.value(item1_ID-1);  // item ID 1 indexed, attributes 0-indexed
            item2_rating = user.value(item2_ID-1);

            numerator = numerator + ((item1_rating - average(user)) * (item2_rating - average(user)));
            denominator1_noRoot = denominator1_noRoot + Math.pow((item1_rating - average(user)),2);
            denominator2_noRoot = denominator2_noRoot + Math.pow((item2_rating - average(user)),2);
        }

        denominator1_root = Math.sqrt(denominator1_noRoot);
        denominator2_root = Math.sqrt(denominator2_noRoot);

        return ((numerator)/(denominator1_root * denominator2_root));
    }

    public static List<Instance> commonUsers(int item1_ID, int item2_ID, Instances ratingsMatrix) {

        List<Instance> commonUsers = new LinkedList<Instance>();
        Instance currentUser;
        double item1_value;
        double item2_value;

        for(int i = 0; i < ratingsMatrix.numInstances(); i++) {
            currentUser = ratingsMatrix.instance(i);
            item1_value = currentUser.value(item1_ID-1); // item ID is 1-indexed while attribute fields are 0-indexed
            item2_value = currentUser.value(item2_ID-1);
            if((item1_value > 0.0) & (item2_value > 0.0)) {
                commonUsers.add(currentUser);   // add to list if current user has rated both item 1 and 2
            }
        }

        return commonUsers;
    }

    public static double average(Instance user) {
        double itemsRated = 0.0;
        double sum = 0.0;
        double item_rating;

        // we exclude the last attribute since that is the user ID and not a rating
        for(int i = 0; i < user.numAttributes()-1; i++) {
            item_rating = user.value(i);
            if(item_rating > 0.0) {
                itemsRated = itemsRated + 1;
                sum = sum + item_rating;
            }
        }

        return (sum/itemsRated);
    }

}

class graphEdge {
    private double weight;

    public graphEdge(double weight) {
        this.weight = weight;
    }

    double getWeight() {
        return weight;
    }

    public String toString() {
        return "Weight " + weight;
    }
}

class graphNode {
    private int userID;     // user ID is 0-indexed

    private int degree;

    private int discountedDegree;     // note that this is the single discounted degree

    private double pu; // this is the uptake probability distribution used in seed selection

    List<item> EPRList;

    public graphNode(int userID) {
        this.userID = userID;
        EPRList = new ArrayList<item>();
        degree = 0;
    }

    int getID() {
        return userID;
    }

    int getDegree() {
        return degree;
    }

    void setDegree(int degree) {
        this.degree = degree;
    }

    void setDiscountedDegree(int discountedDegree) {
        this.discountedDegree = discountedDegree;
    }

    int getDiscountedDegree() {
        return discountedDegree;
    }

    void setPu(double pu) {
        this.pu = pu;
    }

    double getPu(){
        return pu;
    }

    public String toString() {
        return "" + userID;
    }

    void sortEPR() {
        Collections.sort(EPRList, new Comparator<item>() {    // sort the items in the list based on their bid and rate of uptake product
            @Override
            public int compare(item i1, item i2) {
                return Double.compare(i2.getBidUptakeProduct(), i1.getBidUptakeProduct()); // sorts in descending order
            }
        });
    }
}

class item {
    private int itemID;     // this is 1-indexed

    private int numberOfRatings;

    private double itemBid;

    private double rateOfUptake;

    private double payment; // this field is used to hold the price paid to users during a particular iteration for rating the item

    private double budget;

    List<graphNode> seeds;
    List<graphNode> usersList; // a list of users for whom this item has appeared in their EPR List
    List<graphNode> ratedUsersList; // a list of users who have chosen to rate this item

    public item(int itemID, int numberOfRatings) {
        this.itemID = itemID;
        this.numberOfRatings = numberOfRatings;
        seeds = new ArrayList<graphNode>();
        usersList = new ArrayList<graphNode>();
        ratedUsersList = new ArrayList<graphNode>();
        rateOfUptake = 1; // rate of uptake for item is 1 initially
    }

    public int getItemID() {
        return itemID;
    }

    public int getNumberOfRatings(){
        return numberOfRatings;
    }

    public void setItemBid(double itemBid) {
        this.itemBid = itemBid;
    }

    public double getItemBid() {
        return itemBid;
    }

    public void setRateOfUptake(double rateOfUptake) {
        this.rateOfUptake = rateOfUptake;
    }

    public double getBidUptakeProduct() {
        return rateOfUptake*itemBid;
    }

    public double getPayment() {
        return payment;
    }

    public void setPayment(double payment) {
        this.payment = payment;
    }

    public double getBudget() {
        return budget;
    }

    public void setBudget(double budget) {
        this.budget = budget;
    }
}