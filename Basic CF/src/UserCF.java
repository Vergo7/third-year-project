import au.com.bytecode.opencsv.CSVReader;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;
import weka.core.neighboursearch.LinearNNSearch;
import weka.filters.Filter;
import weka.filters.unsupervised.instance.NonSparseToSparse;

import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class UserCF {

    public static void main(String[] args) throws Exception {

        CSVtoARFF("u1base");
        CSVtoARFF("u1test");

        DataSource source = new DataSource("u1base.arff");
        Instances dataset = source.getDataSet();

        source = new DataSource("u1test.arff");
        Instances userRating = source.getDataSet();
        Instance userData = userRating.firstInstance();

        LinearNNSearch kNN = new LinearNNSearch(dataset);
        kNN.setSkipIdentical(true);
        Instances neighbors = null;
        double[] distances = null;

        try {
            neighbors = kNN.kNearestNeighbours(userData, 3);
            distances = kNN.getDistances();
        } catch (Exception e) {
            System.out.println("Neighbors could not be found.");
            return;
        }

        double[] similarities = new double[distances.length];
        for (int i = 0; i < distances.length; i++) {
            similarities[i] = 1.0 / distances[i];
            //System.out.println(similarities[i]);
        }

        Map<String, List<Integer>> recommendations = new HashMap<String, List<Integer>>();
        for(int i = 0; i < neighbors.numInstances(); i++){
            Instance currNeighbor = neighbors.instance(i);

            for (int j = 0; j < currNeighbor.numAttributes(); j++) {
                // item is not ranked by the user, but is ranked by neighbors
                if (userData.value(j) < 1) {
                    // retrieve the name of the movie
                    String attrName = userData.attribute(j).name();
                    List<Integer> lst = new ArrayList<Integer>();
                    if (recommendations.containsKey(attrName)) {
                        lst = recommendations.get(attrName);
                    }

                    lst.add((int)currNeighbor.value(j));
                    recommendations.put(attrName, lst);
                }
            }

        }

        List<RecommendationRecord> finalRanks = new ArrayList<RecommendationRecord>();

        Iterator<String> it = recommendations.keySet().iterator();
        while (it.hasNext()) {
            String atrName = it.next();
            double totalImpact = 0;
            double weightedSum = 0;
            List<Integer> ranks = recommendations.get(atrName);
            for (int i = 0; i < ranks.size(); i++) {
                int val = ranks.get(i);
                totalImpact += similarities[i];
                weightedSum += (double) similarities[i] * val;
            }
            RecommendationRecord rec = new RecommendationRecord();
            rec.attributeName = atrName;
            rec.score = weightedSum / totalImpact;

            finalRanks.add(rec);
        }
        Collections.sort(finalRanks);

        // print top 3 recommendations
        System.out.println(finalRanks.get(0));
        System.out.println(finalRanks.get(1));
        System.out.println(finalRanks.get(2));
    }

    public static void CSVtoARFF(String csvFilename) throws Exception {
        FastVector atts = new FastVector();
        Instances data;

        for(int i = 0; i < 1682; i++) {
            atts.addElement(new Attribute("item_" + (i+1)));
        }

        data = new Instances("MyRelation", atts, 0);

        CSVReader reader = new CSVReader(new FileReader(csvFilename + ".csv"));

        int userIndex = 1;
        double item_id;
        double rating;
        double[] vals = new double[data.numAttributes()];

        String [] nextLine;
        while ((nextLine = reader.readNext()) != null) {
            // nextLine[] is an array of values from the line

            if(Double.parseDouble(nextLine[0]) == userIndex) {
                item_id = Double.parseDouble(nextLine[1]);
                rating = Double.parseDouble(nextLine[2]);

                vals[(int)item_id - 1] = rating;
            } else {
                data.add(new Instance(1.0, vals));
                userIndex++;
                vals = new double[data.numAttributes()];
                item_id = Double.parseDouble(nextLine[1]);
                rating = Double.parseDouble(nextLine[2]);

                vals[(int)item_id - 1] = rating;
            }
        }

        NonSparseToSparse nonSparseToSparseInstance = new NonSparseToSparse();
        nonSparseToSparseInstance.setInputFormat(data);
        Instances sparseDataset = Filter.useFilter(data, nonSparseToSparseInstance);

        BufferedWriter writer = new BufferedWriter(new FileWriter(csvFilename + ".arff"));
        writer.write(sparseDataset.toString());
        writer.flush();
        writer.close();
    }

}

class RecommendationRecord implements Comparable<RecommendationRecord> {
    public double score;
    public String attributeName;
    public int item_index;

    public int compareTo(RecommendationRecord other) {
        if (this.score > other.score)
            return -1;
        if (this.score < other.score)
            return 1;
        return 0;
    }

    public String toString() {
        return attributeName + ": " + score;
    }
}

