\contentsline {chapter}{\numberline {1}Introduction}{5}{chapter.1}
\contentsline {section}{\numberline {1.1}Background}{5}{section.1.1}
\contentsline {section}{\numberline {1.2}Motivation}{6}{section.1.2}
\contentsline {section}{\numberline {1.3}Problem}{7}{section.1.3}
\contentsline {section}{\numberline {1.4}Proposed Solution}{7}{section.1.4}
\contentsline {section}{\numberline {1.5}Scope and Limitations}{7}{section.1.5}
\contentsline {chapter}{\numberline {2}Literature Review}{9}{chapter.2}
\contentsline {section}{\numberline {2.1}Collaborative Filtering}{9}{section.2.1}
\contentsline {section}{\numberline {2.2}Cold-start}{9}{section.2.2}
\contentsline {section}{\numberline {2.3}Algorithms for the new-item problem}{10}{section.2.3}
\contentsline {section}{\numberline {2.4}Comparing algorithms}{11}{section.2.4}
\contentsline {section}{\numberline {2.5}Evaluation}{11}{section.2.5}
\contentsline {chapter}{\numberline {3}Datasets}{12}{chapter.3}
\contentsline {section}{\numberline {3.1}MovieLens}{12}{section.3.1}
\contentsline {section}{\numberline {3.2}Hummingbird}{12}{section.3.2}
\contentsline {chapter}{\numberline {4}Project outline}{14}{chapter.4}
\contentsline {section}{\numberline {4.1}Algorithms selected}{14}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}A Market-based Approach to Address the New Item Problem}{14}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}An Approach for Combining Content-based and Collaborative Filters}{14}{subsection.4.1.2}
\contentsline {subsection}{\numberline {4.1.3}Na\IeC {\"\i }ve Filterbots for Robust Cold-Start Recommendations}{14}{subsection.4.1.3}
\contentsline {section}{\numberline {4.2}Evaluation Process}{15}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Generating new-items}{15}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Item-based collaborative filtering}{15}{subsection.4.2.2}
\contentsline {section}{\numberline {4.3}Evaluation metric}{16}{section.4.3}
\contentsline {chapter}{\numberline {5}Market-based algorithm}{17}{chapter.5}
\contentsline {section}{\numberline {5.1}Overview}{17}{section.5.1}
\contentsline {section}{\numberline {5.2}Influence Network}{17}{section.5.2}
\contentsline {section}{\numberline {5.3}Seed set}{17}{section.5.3}
\contentsline {section}{\numberline {5.4}Rate of uptake}{18}{section.5.4}
\contentsline {section}{\numberline {5.5}Algorithm}{18}{section.5.5}
\contentsline {subsection}{\numberline {5.5.1}Item assignment}{18}{subsection.5.5.1}
\contentsline {subsection}{\numberline {5.5.2}Item selection}{18}{subsection.5.5.2}
\contentsline {subsection}{\numberline {5.5.3}Update}{19}{subsection.5.5.3}
\contentsline {section}{\numberline {5.6}Implementation Details}{19}{section.5.6}
\contentsline {chapter}{\numberline {6}Clustering algorithm}{20}{chapter.6}
\contentsline {section}{\numberline {6.1}Overview}{20}{section.6.1}
\contentsline {section}{\numberline {6.2}Clustering Technique}{20}{section.6.2}
\contentsline {section}{\numberline {6.3}Group ratings matrix}{20}{section.6.3}
\contentsline {section}{\numberline {6.4}Algorithm}{21}{section.6.4}
\contentsline {section}{\numberline {6.5}Implementation Details}{21}{section.6.5}
\contentsline {chapter}{\numberline {7}Na\IeC {\"\i }ve Filterbots}{22}{chapter.7}
\contentsline {section}{\numberline {7.1}Overview}{22}{section.7.1}
\contentsline {section}{\numberline {7.2}Filterbots}{22}{section.7.2}
\contentsline {subsection}{\numberline {7.2.1}Critic-bot}{22}{subsection.7.2.1}
\contentsline {subsection}{\numberline {7.2.2}Award-bot}{22}{subsection.7.2.2}
\contentsline {subsection}{\numberline {7.2.3}Actor-bot}{23}{subsection.7.2.3}
\contentsline {subsection}{\numberline {7.2.4}Director-bot}{23}{subsection.7.2.4}
\contentsline {subsection}{\numberline {7.2.5}Genre-bot}{23}{subsection.7.2.5}
\contentsline {section}{\numberline {7.3}Algorithm}{23}{section.7.3}
\contentsline {section}{\numberline {7.4}Implementation Details}{23}{section.7.4}
\contentsline {chapter}{\numberline {8}Combining algorithms - Clustering and Na\IeC {\"\i }ve Filterbots}{25}{chapter.8}
\contentsline {section}{\numberline {8.1}Motivation}{25}{section.8.1}
\contentsline {section}{\numberline {8.2}Combination process}{25}{section.8.2}
\contentsline {section}{\numberline {8.3}Implementation Details}{25}{section.8.3}
\contentsline {chapter}{\numberline {9}Algorithm Results and Comparison}{27}{chapter.9}
\contentsline {section}{\numberline {9.1}Market-based algorithm}{27}{section.9.1}
\contentsline {section}{\numberline {9.2}Clustering algorithm}{29}{section.9.2}
\contentsline {section}{\numberline {9.3}Na\IeC {\"\i }ve Filterbots}{30}{section.9.3}
\contentsline {section}{\numberline {9.4}Combined method}{30}{section.9.4}
\contentsline {section}{\numberline {9.5}Comparison}{32}{section.9.5}
\contentsline {subsection}{\numberline {9.5.1}MovieLens}{32}{subsection.9.5.1}
\contentsline {subsection}{\numberline {9.5.2}Hummingbird}{32}{subsection.9.5.2}
\contentsline {section}{\numberline {9.6}Summary}{33}{section.9.6}
\contentsline {chapter}{\numberline {10}Project Management}{35}{chapter.10}
\contentsline {section}{\numberline {10.1}Methodology}{35}{section.10.1}
\contentsline {subsection}{\numberline {10.1.1}Algorithm Research}{35}{subsection.10.1.1}
\contentsline {subsection}{\numberline {10.1.2}Dataset Research}{35}{subsection.10.1.2}
\contentsline {subsection}{\numberline {10.1.3}Basic CF Implementation}{36}{subsection.10.1.3}
\contentsline {subsection}{\numberline {10.1.4}Initial Dataset Preparation}{36}{subsection.10.1.4}
\contentsline {subsection}{\numberline {10.1.5}Algorithms Implementation}{36}{subsection.10.1.5}
\contentsline {subsection}{\numberline {10.1.6}Explore Algorithm Extensions}{36}{subsection.10.1.6}
\contentsline {subsection}{\numberline {10.1.7}Prepare Additional Dataset}{36}{subsection.10.1.7}
\contentsline {subsection}{\numberline {10.1.8}Compare performance of algorithms}{37}{subsection.10.1.8}
\contentsline {section}{\numberline {10.2}Project Timeline}{37}{section.10.2}
\contentsline {section}{\numberline {10.3}Hardware}{37}{section.10.3}
\contentsline {section}{\numberline {10.4}Revision Control and Backup}{37}{section.10.4}
\contentsline {chapter}{\numberline {11}Legal, Social and Professional Issues}{39}{chapter.11}
\contentsline {chapter}{\numberline {12}Conclusion}{40}{chapter.12}
\contentsline {section}{\numberline {12.1}Goals}{40}{section.12.1}
\contentsline {section}{\numberline {12.2}Results}{40}{section.12.2}
\contentsline {section}{\numberline {12.3}Achievements}{40}{section.12.3}
\contentsline {section}{\numberline {12.4}Difficulties encountered}{41}{section.12.4}
\contentsline {section}{\numberline {12.5}Lessons Learnt}{41}{section.12.5}
\contentsline {section}{\numberline {12.6}Future Work}{42}{section.12.6}
\contentsline {chapter}{\numberline {13}Acknowledgements}{43}{chapter.13}
\contentsline {section}{\numberline {13.1}Software}{43}{section.13.1}
\contentsline {section}{\numberline {13.2}Hardware}{43}{section.13.2}
\contentsline {section}{\numberline {13.3}People}{43}{section.13.3}
\contentsline {chapter}{Appendices}{47}{section*.3}
\setcounter {tocdepth}{0}
\contentsline {chapter}{\numberline {A}On the Disk}{48}{Appendix.a.A}
\contentsline {chapter}{\numberline {B}Algorithm Results}{49}{Appendix.a.B}
\contentsline {chapter}{\numberline {C}Project Specification}{50}{Appendix.a.C}
\contentsline {chapter}{\numberline {D}Progress Report}{51}{Appendix.a.D}
