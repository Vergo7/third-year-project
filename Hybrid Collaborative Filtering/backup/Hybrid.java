import weka.clusterers.SimpleKMeans;
import weka.core.*;
import weka.core.converters.CSVLoader;
import weka.core.converters.ConverterUtils;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

public class Hybrid {

    static double[][] clusterDistanceMatrix;
    static double[] maxDistanceFromCluster;
    static HashMap<Integer, ArrayList<Item>> similarityVectors = new HashMap<Integer, ArrayList<Item>>();

    public static void main(String[] args) throws Exception {

        Instances itemsDescription = null;
        try {
            File file = new File("./u.item");
            CSVLoader csvLoader = new CSVLoader();
            csvLoader.setFieldSeparator("|");
            csvLoader.setSource(file);
            itemsDescription = csvLoader.getDataSet();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        Instances userRatingTrainingMatrix = null;
        try {
            userRatingTrainingMatrix = ConverterUtils.DataSource.read("u2base.arff");
        } catch (Exception e) {
            e.printStackTrace();
        }

        Instances sortedUsers = null;
        try {
            sortedUsers = ConverterUtils.DataSource.read("u_sorted.csv");
        } catch (Exception e) {
            e.printStackTrace();
        }

        Instances testData = null;
        try {
            testData = ConverterUtils.DataSource.read("u2test.csv");
        } catch (Exception e) {
            e.printStackTrace();
        }

        // remove these attributes as clusterer cannot handle string attributes
        weka.filters.unsupervised.attribute.Remove remove = new Remove();
        remove.setAttributeIndices("2-6");
        remove.setInputFormat(itemsDescription);
        itemsDescription = Filter.useFilter(itemsDescription, remove);

        // use a simple cluster
        weka.clusterers.SimpleKMeans KMeansCluster = new SimpleKMeans();
        KMeansCluster.setNumClusters(5);

        KMeansCluster.buildClusterer(itemsDescription);
        Instances clusterCentroids = KMeansCluster.getClusterCentroids();

        // this method initialises the cluster distance matrix and the maxDistanceFromCluster array
        getClusterDistanceMatrix(itemsDescription, clusterCentroids);

        Instances groupRatingMatrix;
        ArrayList<Attribute> atts = new ArrayList<Attribute>();
        ArrayList<Instance> instances = new ArrayList<Instance>();

        atts.add(new Attribute("item_id")); // adds attribute for item ID to the instance
        for(int i = 0; i < clusterCentroids.numInstances(); i++) {
            atts.add(new Attribute("cluster_" + (i+1)));    // adds each cluster as an attribute for the instance
        }

        for(int i = 0; i < itemsDescription.numInstances(); i++) {
            Instance currentInstance = itemsDescription.get(i); // this is the current instance from the item description

            SparseInstance currentSparseInstance = new SparseInstance(clusterCentroids.numInstances() + 1); // this is the new instance we're adding to the group rating matrix, +1 because of item id as well
            currentSparseInstance.setValue(0, (int) currentInstance.value(0));  // this gets the item ID from current instance
            currentSparseInstance.setValue(1, getProbability(i,0)); // cluster 1's probability (cluster in the ClusterDistanceMatrix is 0-indexed)
            currentSparseInstance.setValue(2, getProbability(i,1)); // cluster 2's probability
            currentSparseInstance.setValue(3, getProbability(i,2)); // cluster 3's probability
            currentSparseInstance.setValue(4, getProbability(i,3)); // cluster 4's probability
            currentSparseInstance.setValue(5, getProbability(i,4)); // cluster 5's probability
            instances.add(currentSparseInstance);   // add this new instance to the instance arraylist
        }

        groupRatingMatrix = new Instances("groupRatingMatrix", atts, instances.size()); // create new matrix with the specified attributes and the number of instances we added above
        for(Instance inst : instances) {
            groupRatingMatrix.add(inst);    // actually add the instance to the matrix
        }

        // gets the item IDs of items that we are considering as new items
        ArrayList<Integer> newItems = newItems(sortedUsers, userRatingTrainingMatrix);


        // for each new item, we create an array that stores the similarity of this item with every other item in the database
        for(int i = 0; i < newItems.size(); i++) {
            int item_ID = newItems.get(i);  // get the item ID of the new item in consideration
            ArrayList<Item> similarityVector = new ArrayList<Item>();   // create a vector to hold the similarity values (stored in the form of an object)
            // go through each item in the database
            for(int j = 0; j < groupRatingMatrix.numInstances(); j++) {
                Instance currentInstance = groupRatingMatrix.instance(j); // the item under consideration
                int hashMapItemID = (int) currentInstance.value(0); // the item ID of the item under consideration

                // we don't want to add the similarity with the NEW item itself
                if(item_ID != hashMapItemID) {
                    // for each item in the database we create a new item object that holds the ID of this item as well as the similarity of it with the NEW item under consideration
                    Item hashMapItem = new Item(hashMapItemID, adjustedCosineSimilarity(item_ID, hashMapItemID, groupRatingMatrix));
                    similarityVector.add(hashMapItem);  // add this item to the similarity vector
                }
            }

            Collections.sort(similarityVector, new Comparator<Item>() {    // sort the items in the list based on their similarities to the item in question
                @Override
                public int compare(Item i1, Item i2) {
                    return Double.compare(i2.getSimilarity(), i1.getSimilarity()); // sorts in descending order
                }
            });

            similarityVectors.put(item_ID, similarityVector); // put this sorted list in the map under the key of the NEW item
        }

        double meanAverageError = 0;
        double numberOfPredictions = 0;

        for(int i = 0; i < testData.numInstances(); i++) {
            Instance currentInstance = testData.instance(i);
            if(newItems.contains((int) currentInstance.value(1))) {
                int user_ID = (int) currentInstance.value(0);
                int item_ID = (int) currentInstance.value(1);

                double rating = currentInstance.value(2);
                double predictedRating = predictedRating(user_ID, item_ID, userRatingTrainingMatrix);

                meanAverageError += Math.abs(predictedRating-rating);
                numberOfPredictions++;

                System.out.println("User " + user_ID + " rated item " + item_ID + " = " + rating + " and it was predicted as = " + predictedRating);
            }
        }

        System.out.println("Mean average error is " + (meanAverageError/numberOfPredictions));
    }

    // intialises both the cluster distance matrix and the maxDistanceFromCluster
    static void getClusterDistanceMatrix(Instances itemsDescription, Instances clusterCentroids) throws Exception {
        clusterDistanceMatrix = new double[itemsDescription.numInstances()][clusterCentroids.numInstances()];
        maxDistanceFromCluster = new double[clusterCentroids.numInstances()];

        EuclideanDistance euclideanDistance = new EuclideanDistance();
        euclideanDistance.setAttributeIndices("2-last");    // we don't want to take item ID into consideration while computing distance
        euclideanDistance.setInstances(itemsDescription);   // this is the instance format that the distance will be calculated on

        for(int i = 0; i < itemsDescription.numInstances(); i++) {
            Instance currentInstance = itemsDescription.instance(i);    // each item represents a row in the matrix
            for(int j = 0; j < clusterCentroids.numInstances(); j++) {
                Instance currentCluster = clusterCentroids.instance(j); // each cluster represents a column in the matrix
                double distance = euclideanDistance.distance(currentInstance, currentCluster);  // calculate the distance between current instance and cluster
                clusterDistanceMatrix[i][j] = distance; // store this value in the matrix
                if(distance > maxDistanceFromCluster[j]) {
                    maxDistanceFromCluster[j] = distance;   // if this distance is larger than the max. distance we've observed for this cluster till now, replace max. distance
                }
            }
        }
    }

    static double getProbability(int j, int k) {
        double probability = 1 - (clusterDistanceMatrix[j][k])/(maxDistanceFromCluster[k]);
        return probability;
    }

    // this method gives the list of items that we are considering as new items and also sets the ratings for all these items to 0 to simulate the fact that it's a new item
    static ArrayList<Integer> newItems(Instances sortedUsers, Instances userRatingTrainingMatrix) {
        ArrayList<Integer> newItems = new ArrayList<Integer>();

        int ID = 1;
        int total = 0;  // variable to store the number of ratings for a particular item

        for(int i = 0; i < sortedUsers.numInstances(); i++) {
            if(ID == sortedUsers.instance(i).value(1)) {
                total++;
                continue;   // this ensures that we don't get to bottom part till counting has been completed
            }

            if(total > 350) {
                newItems.add(ID);
                // go through all rows and set the ratings of this item to 0 (essentially making it behave like a new item)
                for(int j = 0; j < userRatingTrainingMatrix.numInstances(); j++) {
                    // ID-1 because ID is 1-indexed while the attributes are 0-indexed
                    userRatingTrainingMatrix.instance(j).setValue(ID-1, 0);
                }
            }

            total = 1;  // breaking out of initial if statement means that we have moved on to new ID, so need to set total as 1 to count the current new item
            ID++;
        }

        return newItems;
    }


    static double adjustedCosineSimilarity(int itemID_K, int itemID_L, Instances groupRatingMatrix) {
        Instance itemK = groupRatingMatrix.instance(itemID_K-1); // item ID is one-indexed while attributes are 0-indexed
        Instance itemL = groupRatingMatrix.instance(itemID_L-1);
        double numerator = 0;
        double denominatior_first = 0;
        double denominator_second = 0;

        // we want to go through all the clusters, but since first attribute is the item ID we skip that
        for(int i = 1; i < groupRatingMatrix.numAttributes(); i++) {
            double itemK_rating = itemK.value(i);   // we essentially consider each cluster to be a separate user
            double itemL_rating = itemL.value(i);
            double average = averageUserRating(i, groupRatingMatrix);   // this gives the average rating of the user (cluster)
            numerator += (itemK_rating - average)*(itemL_rating - average);
            denominatior_first += Math.pow((itemK_rating - average), 2);
            denominator_second += Math.pow((itemL_rating - average), 2);
        }

        denominatior_first = Math.sqrt(denominatior_first);
        denominator_second = Math.sqrt(denominator_second);

        return ((numerator)/((denominatior_first)*(denominator_second)))*0.4;
    }

    // returns the average rating of a cluster in the group rating matrix
    static double averageUserRating(int clusterNumber, Instances groupRatingMatrix) {

        double totalRating = 0;
        double numberOfRatings = 0;

        // go through each item in the database
        for(int i = 0; i < groupRatingMatrix.numInstances(); i++) {
            Instance currentInstance = groupRatingMatrix.instance(i);
            totalRating += currentInstance.value(clusterNumber); // cluster 1 corresponds to index 1 in the group rating matrix, same for the other clusters
            numberOfRatings++;
        }

        return (totalRating)/(numberOfRatings);
    }

    // returns the average rating of an item in the training matrix
    static double averageItemRatingTrainingMatrix(int item_ID, Instances userRatingTrainingMatrix) {
        double totalRating = 0;
        double numberOfRatings = 0;
        // go through each user's ratings in the matrix
        for(int i = 0; i < userRatingTrainingMatrix.numInstances(); i++) {
            Instance currentInstance = userRatingTrainingMatrix.instance(i);    // get the current user
            double currentRating = currentInstance.value(item_ID-1);    // item is 1-indexed while attributes are 0-indexed
            // we only take users who have actually rated the item into account for calculating average
            if(currentRating > 0) {
                totalRating += currentRating;
                numberOfRatings++;
            } else {
                continue;
            }
        }

        if(numberOfRatings > 0) {
            return (totalRating) / (numberOfRatings);
        } else {
            // accounting for case if nearest neighbour of one of the new items is itself a new item, and thus does not have any ratings
            return 0;
        }
    }

    // returns the rating predicted by the algorithm for an item
    static double predictedRating(int user_ID, int item_ID, Instances userRatingTrainingMatrix) {
        ArrayList<Item> similarityVector = similarityVectors.get(item_ID);  // gets the similarity vector corresponding to the new item in question

        int numberOfNeighbours = 4; // setting 5 nearest neighbours for now, can be changed


/*        double averageNeighbourRating = 0;

        for(int i = 0; i < similarityVector.size(); i++) {
            int nearestNeighbour_ID = similarityVector.get(0).getItem_ID();
            double nearestNeighbourRating = userRatingTrainingMatrix.instance(user_ID-1).value(nearestNeighbour_ID-1);
            if(nearestNeighbourRating > 0) {
                averageNeighbourRating = averageItemRatingTrainingMatrix(nearestNeighbour_ID, userRatingTrainingMatrix);
                break;
            }
        }*/

        double numerator = 0;
        double denominator = 0;

        int numberOfRaters = 0;

        // we go through each of the nearest neighbours who have actually rated the item in turn
        for(int i = 0; i < similarityVector.size(); i++) {
            int neighbour_ID = similarityVector.get(i).getItem_ID();    // get the item ID of the neighbour under consideration
            double neighbourRating = userRatingTrainingMatrix.instance(user_ID-1).value(neighbour_ID-1);    // the rating that the user gave the nearest neighbour

            if(neighbourRating > 0) {
                double similarity = similarityVector.get(i).getSimilarity();    // get how similar the neighbour is to the item under consideration

                // numerator += ((neighbourRating - averageItemRatingTrainingMatrix(neighbour_ID, userRatingTrainingMatrix)) * similarity);

                numerator += neighbourRating * similarity;
                denominator += Math.abs(similarity);

                numberOfRaters++;
            }

            if(numberOfRaters == numberOfNeighbours) {
                break;
            }
        }

        // return averageNeighbourRating + (numerator/denominator);
        return (numerator/denominator);
    }
}

class Item {
    private int item_ID;
    private double similarity;

    public Item(int item_ID, double similarity) {
        this.item_ID = item_ID;
        this.similarity = similarity;
    }


    public int getItem_ID() {
        return item_ID;
    }

    public double getSimilarity() {
        return similarity;
    }
}