import org.apache.commons.math3.stat.descriptive.moment.Mean;
import org.apache.commons.math3.stat.descriptive.moment.StandardDeviation;

/**
 * Created by Varun on 21/02/2015.
 */
public class marketBasedHBTest {
    public static void main(String[] args) throws Exception {
        double deltaP = 0;
        double deltaR = 0;
        int numberOfNeighbours = 0;
        double maxBidValue = 100.0;
        int maxCyclesSkip = 10;

        for (int neighbourIteration = 2; neighbourIteration < 4; neighbourIteration++) {

            switch (neighbourIteration) {
                case 0:
                    numberOfNeighbours = 5;
                    break;
                case 1:
                    numberOfNeighbours = 10;
                    break;
                case 2:
                    numberOfNeighbours = 20;
                    break;
                case 3:
                    numberOfNeighbours = 40;
                    break;
            }

            System.out.println("Number of neighbours = " + numberOfNeighbours);

            for (int deltaPIteration = 0; deltaPIteration < 4; deltaPIteration++) {

                switch (deltaPIteration) {
                    case 0:
                        deltaP = 0.3;
                        break;
                    case 1:
                        deltaP = 0.6;
                        break;
                    case 2:
                        deltaP = 0.9;
                        break;
                    case 3:
                        deltaP = 1.2;
                }
                for (int deltaRIteration = 0; deltaRIteration < 5; deltaRIteration++) {

                    switch (deltaRIteration) {
                        case 0:
                            deltaR = 0.2;
                            break;
                        case 1:
                            deltaR = 0.4;
                            break;
                        case 2:
                            deltaR = 0.6;
                            break;
                        case 3:
                            deltaR = 0.8;
                            break;
                        case 4:
                            deltaR = 1;
                    }

                    System.out.println("");
                    System.out.println("For deltaP = " + deltaP + " and deltaR = " + deltaR);

                    int numberOfIterations = 100;

                    double[] individualMAEs = new double[numberOfIterations];

                    for (int i = 0; i < numberOfIterations; i++) {
                        // System.out.println("Iteration " + (i + 1));
                        markedBasedHB.init(deltaP, deltaR, numberOfNeighbours, maxBidValue, maxCyclesSkip);
                        double iterationMAE = markedBasedHB.execute();
                        individualMAEs[i] = iterationMAE;
                    }

                    Mean mean = new Mean();
                    StandardDeviation standardDeviation = new StandardDeviation();

                    System.out.println("");
                    System.out.println("Mean = " + mean.evaluate(individualMAEs));
                    System.out.println("S.D = " + standardDeviation.evaluate(individualMAEs));
                }
            }
        }
    }
}
