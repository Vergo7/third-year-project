import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.SparseInstance;
import weka.core.converters.ArffLoader;
import weka.core.converters.ConverterUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class naiveFiltHB {

    public static void main(String[] args) throws Exception {
        Instances u1base = null;

        try {
            File file = new File("./hummingbirdBase1.arff");
            ArffLoader arffLoader = new ArffLoader();
            arffLoader.setSource(file);
            u1base = arffLoader.getDataSet();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        Instances items = null;

        try {
            File file = new File("./itemsWithGenres.arff");
            ArffLoader arffLoader = new ArffLoader();
            arffLoader.setSource(file);
            items = arffLoader.getDataSet();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        Instances userRatingTrainingMatrix = null;
        try {
            File file = new File("./hummingbirdBase1withgenre.arff");
            ArffLoader arffLoader = new ArffLoader();
            arffLoader.setSource(file);
            userRatingTrainingMatrix = arffLoader.getDataSet();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Instances ratingsSortedByItems = null;
        try {
            ratingsSortedByItems = ConverterUtils.DataSource.read("hummingbirdFullSortedByItem.arff");
        } catch (Exception e) {
            e.printStackTrace();
        }

        Instances testData = null;
        try {
            testData = ConverterUtils.DataSource.read("hummingbirdTest1.csv");
        } catch (Exception e) {
            e.printStackTrace();
        }

        /************************************************************************************************************************************************/

/*        HashMap<Integer, Double> genreRatings = genreRatings(items, u1base);
        genreBot(genreRatings, items, u1base);*/

        ArrayList<Integer> newItems = newItems(ratingsSortedByItems, userRatingTrainingMatrix);

        System.out.println("Got new items");
        System.out.println(newItems.size() + " items selected as new ones");

        double meanAverageError = 0;
        double numberOfPredictions = 0;
        HashMap<Integer, double[]> newItemSimilarityMatrix = createNewItemSimilarityMatrix(newItems, userRatingTrainingMatrix, ratingsSortedByItems);

        System.out.println("Created similarity matrix");

        for(int i = 0; i < testData.numInstances(); i++) {
            Instance currentInstance = testData.instance(i);
            if(newItems.contains((int) currentInstance.value(1))) {
                int user_ID = (int) currentInstance.value(0);
                int item_ID = (int) currentInstance.value(1);

                double rating = currentInstance.value(2);
                double predictedRating = predictedRating(user_ID, item_ID, userRatingTrainingMatrix, ratingsSortedByItems, newItemSimilarityMatrix);

                meanAverageError += Math.abs(predictedRating - rating);
                numberOfPredictions++;

                System.out.println("User " + user_ID + " rated item " + item_ID + " = " + rating + " and it was predicted as = " + predictedRating);
            }
        }
        System.out.println("Mean average error is " + (meanAverageError/numberOfPredictions));
    }


    /************************************************************************************************************************************************/

    // genres start from index 2 and go up till the end

    static void genreBot(HashMap<Integer, Double> genreRatings, Instances items, Instances u1base) throws Exception {
        SparseInstance genreInstance = new SparseInstance(u1base.numAttributes()); // new instance to store ratings of genreBot

        // leaving out last attribute as it is user ID
        for(int i = 0; i < u1base.numAttributes()-1; i++) {
            int itemID = i+1; // item ID is one more than attribute index

            double totalRating = 0;
            double totalItems = 0;
            Instance itemInstance = items.instance(itemID-1);   // get corresponding item from item information matrix

            // go through each genre and check which ones the movie belongs to
            // note than HB genres start from index 2
            for(int genreIndex = 2; genreIndex < items.numAttributes(); genreIndex++) {
                // if the movie belongs to the genre, get the genre rating
                if(itemInstance.value(genreIndex) == 1) {
                    double genreRating = genreRatings.get(genreIndex);
                    // we ignore the genre rating if it is 0, i.e, no one has rated the corresponding movies
                    if(genreRating > 0) {
                        totalRating += genreRating;
                        totalItems++;
                    }
                }
            }

            if(totalItems > 0) {
                genreInstance.setValue(i, (totalRating/totalItems));
            } else {
                System.out.println("item " + i + " has no genre information");
                genreInstance.setValue(i, 0);   // accounting for case where no one has rated movies of that genre
            }
        }

        genreInstance.setValue(u1base.numAttributes()-1, u1base.numInstances()+1);
        u1base.add(genreInstance);

        BufferedWriter writer = new BufferedWriter(new FileWriter("hummingbirdBase1withgenre.arff"));
        writer.write(u1base.toString());
        writer.flush();
        writer.close();
    }

    static HashMap<Integer, Double> genreRatings(Instances items, Instances u1base) {
        // hashmap to store genre ratings
        HashMap<Integer, Double> genreRatings = new HashMap<Integer, Double>();
        // go through each genre, calculate rating for each one and store it in hashmap
        // in contrast to movielens naive filterbots (where it starts with index 7), genres start from attribute index 2 in the HB items data
        for(int i = 2; i < items.numAttributes(); i++) {
            genreRatings.put(i, getGenreRating(items, u1base, i));
        }

        return genreRatings;
    }

    static double getGenreRating(Instances items, Instances u1base, int genreIndex) {
        double totalRating = 0;
        double totalItems = 0;

        // go through all the hummingbird items
        for(int i = 0; i < items.numInstances(); i++) {
            Instance currentInstance = items.instance(i);
            // check if current item belongs to the genre we're getting the rating for
            if(currentInstance.value(genreIndex) == 1) {
                int itemID = (int) currentInstance.value(0);
                // if it does, get the average rating for this item
                double averageRating = averageItemRating(itemID, u1base);
                // ignore the item if it has no ratings, i.e, it is a new item
                if(averageRating > 0) {
                    totalRating += averageRating;
                    totalItems++;
                }
            }
        }

        if(totalItems > 0) {
            return (totalRating/totalItems);
        } else {
            return 0; // accounting for the case where all movies of this genre have no ratings
        }
    }

    /************************************************************************************************************************************************/

    public static double predictedRating(int userID, int itemID, Instances userRatingTrainingMatrix, Instances ratingsSortedByItems, HashMap<Integer, double[]> newItemSimilarityMatrix) {
        double numerator = 0.0;
        double denominator = 0.0;
        double similarity;
        double ratedItem_rating;
        double finalRating;

        Instance user = userRatingTrainingMatrix.instance(userID-1); // as noted above, this user ID is 1-indexed while instances field is 0-indexed

        // we skip the final attribute since it's the user ID
        for(int i = 0; i < userRatingTrainingMatrix.numAttributes()-1; i++) {

            if((itemID-1) == i) {
                continue;   // don't want to include the item itself when approximating score
            }

            int item2_ID = i+1; // the ID of the second item is one more than its attribute index

            similarity = newItemSimilarityMatrix.get(itemID)[item2_ID-1]; // gets row of similarities to other items from hashmap by item ID, after that Item IDs 1-indexed and array row fields 0-indexed

            // if similarity between the two items exists and the user has rated this other item
            // similarity threshold seems to greatly reduce the MAE
            if((Double.isNaN(similarity) == false) && (user.value(item2_ID - 1) > 0) && (similarity > 0.5)) {

                ratedItem_rating = normalize(user.value(item2_ID - 1));
                numerator = numerator + (similarity * ratedItem_rating);
                denominator = denominator + Math.abs(similarity);
            }
        }


        if((numerator > 0 == false) && (denominator > 0 == false)) {
            System.out.println("AVERAGE IS BEING USED");
            return userAverage(user, ratingsSortedByItems);
        }

        finalRating = numerator/denominator;
        return denormalize(finalRating);
    }

    // note that max HB rating is 10 instead of 5 as in movielens

    public static double normalize(double rating) {
        double min_rating = 1.0;
        double max_rating = 10.0;
        double normalized_rating;

        normalized_rating = (2*(rating - min_rating) - (max_rating- min_rating))/(max_rating - min_rating);

        return normalized_rating;
    }

    public static double denormalize(double rating) {
        double min_rating = 1.0;
        double max_rating = 10.0;
        double denormalized_rating;

        denormalized_rating = ((rating + 1) * (max_rating - min_rating))/2 + min_rating;

        return denormalized_rating;
    }

    public static HashMap<Integer, double[]> createNewItemSimilarityMatrix(ArrayList<Integer> newItems, Instances userRatingTrainingMatrix, Instances ratingsSortedByItems) {
        HashMap<Integer, double[]> newItemSimilarityMatrix = new HashMap<Integer, double[]>();
        double[] similarityRow;

        for(int i = 0; i < newItems.size(); i++) {
            System.out.println("Filling in item similarity for new item " + (i+1));
            int newItemID = newItems.get(i);
            similarityRow = new double[userRatingTrainingMatrix.numAttributes()-1]; // attributes - 1 because we don't want to include user ID
            for(int j = 0; j < similarityRow.length; j++) {
                similarityRow[j] = itemSimilarity(newItemID, j+1, userRatingTrainingMatrix, ratingsSortedByItems); // item ID is 1-indexed while attribute numbers are 0-indexed
            }

            newItemSimilarityMatrix.put(newItemID, similarityRow);
        }

        return newItemSimilarityMatrix;
    }

    public static double itemSimilarity(int item1_ID, int item2_ID, Instances userRatingTrainingMatrix, Instances ratingsSortedByItems) {
        List<Instance> commonUsers = commonUsers(item1_ID, item2_ID, userRatingTrainingMatrix); // list of users who have rated both these items
        double numerator = 0.0;
        double denominator1_noRoot = 0.0;
        double denominator2_noRoot = 0.0;
        double denominator1_root;
        double denominator2_root;
        double item1_rating;
        double item2_rating;

        // refer to pg. 17 of "http://guidetodatamining.com/guide/ch3/DataMining-ch3.pdf" for algorithm

        for(Instance user : commonUsers) {
            item1_rating = user.value(item1_ID-1);  // item ID 1 indexed, attributes 0-indexed
            item2_rating = user.value(item2_ID-1);

            numerator = numerator + ((item1_rating - userAverage(user, ratingsSortedByItems)) * (item2_rating - userAverage(user, ratingsSortedByItems)));
            denominator1_noRoot = denominator1_noRoot + Math.pow((item1_rating - userAverage(user, ratingsSortedByItems)),2);
            denominator2_noRoot = denominator2_noRoot + Math.pow((item2_rating - userAverage(user, ratingsSortedByItems)),2);
        }

        denominator1_root = Math.sqrt(denominator1_noRoot);
        denominator2_root = Math.sqrt(denominator2_noRoot);

        return ((numerator)/(denominator1_root * denominator2_root));
    }

    static List<Instance> commonUsers(int item1ID, int item2ID, Instances userRatingTrainingMatrix) {
        List<Instance> commonUsers = new LinkedList<Instance>();
        Instance currentUser;
        double item1_value;
        double item2_value;

        for(int i = 0; i < userRatingTrainingMatrix.numInstances(); i++) {
            currentUser = userRatingTrainingMatrix.instance(i);
            item1_value = currentUser.value(item1ID-1); // item ID is 1-indexed while attribute fields are 0-indexed
            item2_value = currentUser.value(item2ID-1);
            if((item1_value > 0.0) & (item2_value > 0.0)) {
                commonUsers.add(currentUser);   // add to list if current user has rated both item 1 and 2
            }
        }

        return commonUsers;
    }

    static double userAverage(Instance user, Instances ratingsSortedByItems) {
        double itemsRated = 0.0;
        double sum = 0.0;
        double item_rating;

        // we exclude the last attribute since that is the user ID and not a rating
        for(int i = 0; i < user.numAttributes()-1; i++) {
            item_rating = user.value(i);
            if(item_rating > 0.0) {
                itemsRated = itemsRated + 1;
                sum = sum + item_rating;
            }
        }

        if(itemsRated > 0 == false) {
            // return ratingsSortedByItems.meanOrMode(2);  // if no average rating for item is available return global mean rating
        }

        return (sum/itemsRated);
    }

    static ArrayList<Integer> newItems(Instances ratingsSortedByItems, Instances userRatingTrainingMatrix) {
        ArrayList<Integer> newItems = new ArrayList<Integer>();

        int ID = 1;
        int total = 0; // variable to store the number of ratings for a particular item

        for(int i = 0; i < ratingsSortedByItems.numInstances(); i++) {
            if(ID == ratingsSortedByItems.instance(i).value(1)) {
                total++;
                // if it is the last user entry we're looking at we must definitely execute the below code
                if(i != ratingsSortedByItems.numInstances()-1) {
                    continue;   // this ensures that we don't get to bottom part till counting has been completed
                }
            }

            if(total > 400) {   // total = 400 returns 24 new items
                newItems.add(ID);
                // go through all rows and set the ratings of this item to 0 (essentially making it behave like a new item)
                // last instance is the genre bot ratings which we should leave untouched
                for(int j = 0; j < userRatingTrainingMatrix.numInstances()-1; j++) {
                    // ID - 1 because ID is 1-indexed while the attributes are 0 indexed
                    userRatingTrainingMatrix.instance(j).setValue(ID-1, 0);
                }
            }

            total = 1; // breaking out of initial if statement means that we have moved on to new ID, so need to set total as 1 to count the current new item
            ID++;
        }

        return newItems;
    }

    /************************************************************************************************************************************************/

    static double averageItemRating(int itemID, Instances u1base) {
        double totalRating = 0;
        double totalItems = 0;

        for(int i = 0; i < u1base.numInstances(); i++) {
            Instance currentInstance = u1base.instance(i);
            double rating = currentInstance.value(itemID-1); // item ID is one indexed while attributes are 0-indexed

            if(rating > 0) {
                totalRating += rating;
                totalItems++;
            }
        }

        if(totalItems > 0) {
            return (totalRating/totalItems);
        } else {
            return 0;
        }
    }
}
