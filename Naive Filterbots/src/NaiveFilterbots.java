import weka.core.Instance;
import weka.core.Instances;
import weka.core.SparseInstance;
import weka.core.converters.ArffLoader;
import weka.core.converters.CSVLoader;
import weka.core.converters.ConverterUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.*;

/**
 * Created by Varun on 26/12/2014.
 */
public class NaiveFilterbots {

    public static void main(String[] args) throws Exception {
        Instances u1base = null;

        try {
            File file = new File("./u1base.arff");
            ArffLoader arffLoader = new ArffLoader();
            arffLoader.setSource(file);
            u1base = arffLoader.getDataSet();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        Instances items = null;

        try {
            File file = new File("./combinedIDs.arff");
            ArffLoader arffLoader = new ArffLoader();
            arffLoader.setSource(file);
            items = arffLoader.getDataSet();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        Instances yahooDescription = null;

        try {
            File file = new File("./yahoo_description");
            CSVLoader csvLoader = new CSVLoader();
            csvLoader.setFieldSeparator("\t");
            csvLoader.setSource(file);
            yahooDescription = csvLoader.getDataSet();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        Instances userRatingTrainingMatrix = null;
        try {
            userRatingTrainingMatrix = ConverterUtils.DataSource.read("u1basewithfilterbots.arff");
        } catch (Exception e) {
            e.printStackTrace();
        }

        Instances sortedMappedUsers = null;
        try {
            sortedMappedUsers = ConverterUtils.DataSource.read("u_sortedOnlyMapped.csv");
        } catch (Exception e) {
            e.printStackTrace();
        }

        Instances testData = null;
        try {
            testData = ConverterUtils.DataSource.read("u1test.csv");
        } catch (Exception e) {
            e.printStackTrace();
        }

        /************************************************************************************************************************************************/

/*        HashMap<Integer, Double> actorRatings = actorRatings(items, yahooDescription, u1base);
        ActorBot(actorRatings, items, yahooDescription, u1base);

        HashMap<Integer, Double> directorRatings = directorRatings(items, yahooDescription, u1base);
        DirectorBot(directorRatings, items, yahooDescription, u1base);

        HashMap<Integer, ArrayList<Integer>> awardsPartition = awardsPartition(items, yahooDescription, u1base);
        awardsBot(awardsPartition, u1base);

        criticBot(items, yahooDescription, u1base);

        HashMap<Integer, Double> genreRatings = genreRatings(items, u1base);
        genreBot(genreRatings, items, u1base);*/

        ArrayList<Integer> newItems = newItems(sortedMappedUsers, userRatingTrainingMatrix);

        double meanAverageError = 0;
        double numberOfPredictions = 0;
        HashMap<Integer, double[]> newItemSimilarityMatrix = createNewItemSimilarityMatrix(newItems, userRatingTrainingMatrix);

        for(int i = 0; i < testData.numInstances(); i++) {
            Instance currentInstance = testData.instance(i);
            if(newItems.contains((int) currentInstance.value(1))) {
                int user_ID = (int) currentInstance.value(0);
                int item_ID = (int) currentInstance.value(1);

                double rating = currentInstance.value(2);
                double predictedRating = predictedRating(user_ID, item_ID, userRatingTrainingMatrix, newItemSimilarityMatrix);

                meanAverageError += Math.abs(predictedRating - rating);
                numberOfPredictions++;

                System.out.println("User " + user_ID + " rated item " + item_ID + " = " + rating + " and it was predicted as = " + predictedRating);
            }
        }
        System.out.println("Mean average error is " + (meanAverageError/numberOfPredictions));

    }

    /************************************************************************************************************************************************/

    static void ActorBot (HashMap<Integer, Double> actorRatings, Instances items, Instances yahooDescription, Instances u1base) throws Exception {
        SparseInstance actorInstance = new SparseInstance(u1base.numAttributes());  // create new instance to insert into ratings matrix
        for(int i = 0; i < actorInstance.numAttributes()-1; i++) {
            int currentItemMovieLensID = i+1;   // item ID is one more than it's attribute index
            int currentItemYahooID = (int) items.instance(currentItemMovieLensID-1).value(1);   // get corresponding yahoo ID for the movielens ID

            int[] actorIDs = getActorIDs(currentItemYahooID, yahooDescription); // get the actors for the movie in question

            if(actorIDs == null) {
                actorInstance.setValue(i, 0);
                continue;   // if the movie doesn't contain any actor information just set the rating to 0 and go on to the next one
            }

            double totalRating = 0;
            double totalNumber = 0;

            // go through the first 5 actors for the movie
            for(int arrayIndex = 0; arrayIndex < actorIDs.length; arrayIndex++) {
                if(arrayIndex == 5) {
                    break;
                }

                int currentActorID = actorIDs[arrayIndex];
                double actorRating = actorRatings.get(currentActorID);

                // take into account actor's rating only if it is above 0, i.e, movies in which actor are have been rated
                if(actorRating > 0) {
                    totalRating += actorRating;
                    totalNumber++;
                }
            }

            if(totalNumber > 0) {
                actorInstance.setValue(i, (totalRating/totalNumber));
            } else {
                actorInstance.setValue(i, 0);   // if movies actor is in haven't been rated set the actor rating to 0
            }
        }
        actorInstance.setValue(actorInstance.numAttributes()-1, u1base.numInstances()+1);   // set the user ID for the actor to be one more than the last one in the current matrix
        u1base.add(actorInstance);  // add this bot to the ratings matix

        BufferedWriter writer = new BufferedWriter(new FileWriter("u1basewithactor.arff"));
        writer.write(u1base.toString());
        writer.flush();
        writer.close();
    }

    static HashMap<Integer, Double> actorRatings(Instances items, Instances yahooDescription, Instances u1base) {
        // hashmap to store the actor rating for each actor
        HashMap<Integer, Double> actorRatings = new HashMap<Integer, Double>();

        // go through all the movielens items
        for(int i = 0; i < items.numInstances(); i++) {
            System.out.println("On item " + i);
            Instance currentItem = items.instance(i);
            int currentItemYahooID = (int) currentItem.value(1);

            if(currentItem.isMissing(currentItem.attribute(1))) {
                continue;   // if item doesn't have a corresponding yahoo ID then just skip it as it is of no use
            }

            int[] actorIDs = getActorIDs(currentItemYahooID, yahooDescription);
            if(actorIDs == null) {
                continue;   // if movie doesn't have actor information then just go on to the next one
            }

            for(int arrayIndex = 0; arrayIndex < actorIDs.length; arrayIndex++) {

                if(arrayIndex == 5) {
                    break; // we only consider first 5 actors for any movie
                }

                int currentActorID = actorIDs[arrayIndex];
                if(actorRatings.containsKey(currentActorID) == false) {
                    actorRatings.put(currentActorID, getActorRating(currentActorID, yahooDescription, items, u1base));
                    // System.out.println("Actor rating for actor " + currentActorID + " is " + actorRatings.get(currentActorID));
                }
            }
        }

        return actorRatings;
    }

    static double getActorRating(int actorID, Instances yahooDescription, Instances items, Instances u1base) {

        double totalRating = 0;
        double totalMovies = 0;

        // go through all the movielens items
        for(int i = 0; i < items.numInstances(); i++) {
            Instance currentItem = items.instance(i);
            int currentItemYahooID = (int) currentItem.value(1);
            int currentItemMovieLensID = (int) currentItem.value(0);

            if(currentItem.isMissing(currentItem.attribute(1))) {
                continue;   // if item doesn't have a corresponding yahoo ID then just skip it as it is of no use
            }

            int[] actorIDs = getActorIDs(currentItemYahooID, yahooDescription);
            if(actorIDs == null) {
                continue;   // if the movie doesn't have any actor information then just go on to the next one
            }

            // go through all the actor IDs to check whether the the actor we're looking for is there
            for(int arrayIndex = 0; arrayIndex < actorIDs.length; arrayIndex++) {

                if(arrayIndex == 5) {
                    break; // we only consider first 5 actors for any movie
                }

                int currentActorID = actorIDs[arrayIndex];
                // if the movie in question features the actor who's rating we're currently finding
                if(currentActorID == actorID) {
                    // get the average rating for this movie, returns -1 if no one has rated it (i.e, it is a new item)
                    double averageMovieRating = averageItemRating(currentItemMovieLensID, u1base);
                    if(averageMovieRating != 0) {
                        totalRating += averageMovieRating;
                        totalMovies++;
                    }
                    break;  // don't need to check rest of the actors once we've found the one we wanted
                }
            }
        }
        if(totalMovies > 0) {
            return (totalRating / totalMovies);
        } else {
            return 0;   // the training matrix does not ensure that each item has ratings, so must account for no ratings for an item
        }
    }

    static int[] getActorIDs(int yahooID, Instances yahooDescription) {
        Instance yahooInstance = getInstanceByYahooID(yahooID, yahooDescription);
        if(yahooInstance == null) {
            return null;    // case where there is no corresponding yahoo information for the yahoo movie ID
        }

        // vertical bar separated list of the actors of the movie
        String actorIDsString = yahooInstance.stringValue(2);

        // split by vertical bar to get array of actor IDs for movie
        String[] actorIDsStringSplit = actorIDsString.split("\\|"); // vertical bar needs to be escaped

        try {
            Integer.parseInt(actorIDsStringSplit[0]);
        } catch (Exception e) {
            return null; // case where no actor information is provided
        }

        int[] actorIDs = new int[actorIDsStringSplit.length];
        for(int arrayIndex = 0; arrayIndex < actorIDs.length; arrayIndex++) {
            actorIDs[arrayIndex] = Integer.parseInt(actorIDsStringSplit[arrayIndex]);
        }

        return actorIDs;
    }

    /************************************************************************************************************************************************/

    static void DirectorBot(HashMap<Integer, Double> directorRatings, Instances items, Instances yahooDescription, Instances u1base) throws Exception {
        SparseInstance directorInstance = new SparseInstance(u1base.numAttributes());
        // skipping last attribute as it is user ID
        for(int i = 0; i < directorInstance.numAttributes()-1; i++) {
            int currentItemMovieLensID = i+1; // item ID is one more than its attribute index
            int currentItemYahooID = (int) items.instance(currentItemMovieLensID-1).value(1);

            int[] directorIDs = getDirectorIDs(currentItemYahooID, yahooDescription);

            if(directorIDs == null) {
                directorInstance.setValue(i,0);
                continue;
            }

            double totalRating = 0;
            double totalItems = 0;

            // go through first 5 directors for the movie
            for(int arrayIndex = 0; arrayIndex < directorIDs.length; arrayIndex++) {
                if(arrayIndex == 5) {
                    break;
                }

                int currentDirectorID = directorIDs[arrayIndex];
                double directorRating = directorRatings.get(currentDirectorID);

                // take into account director's rating only if it is above 0, i.e, movies in which director has directed have been rated
                if(directorRating > 0) {
                    totalRating += directorRating;
                    totalItems++;
                }
            }

            if(totalItems > 0) {
                directorInstance.setValue(i, (totalRating/totalItems));
            } else {
                directorInstance.setValue(i,0); // if movies director is part of haven't been rated set the director rating to 0
            }
        }
        directorInstance.setValue(directorInstance.numAttributes()-1, u1base.numInstances()+1); // set the user ID for the director to be one more than the last one in the current matrix
        u1base.add(directorInstance);

        BufferedWriter writer = new BufferedWriter(new FileWriter("u1basewithdirector.arff"));
        writer.write(u1base.toString());
        writer.flush();
        writer.close();
    }

    static HashMap<Integer, Double> directorRatings(Instances items, Instances yahooDescription, Instances u1base) {
        // hashmap to store the director rating for each director
        HashMap<Integer, Double> directorRatings = new HashMap<Integer, Double>();

        // go through all the movielens items
        for(int i = 0; i < items.numInstances(); i++) {
            System.out.println("On item " + i);
            Instance currentItem = items.instance(i);
            int currentItemYahooID = (int) currentItem.value(1);

            if(currentItem.isMissing(currentItem.attribute(1))) {
                continue; // if item doesn't have a corresponding yahoo ID then just skip it as it is of no use
            }

            int[] directorIDs = getDirectorIDs(currentItemYahooID, yahooDescription);
            if(directorIDs == null) {
                continue; // if movie doesn't have director information then just go on to the next one
            }

            for(int arrayIndex = 0; arrayIndex < directorIDs.length; arrayIndex++) {
                if(arrayIndex == 5) {
                    break; // we only consider first 5 directors for any movie
                }

                int currentDirectorID = directorIDs[arrayIndex];
                if(directorRatings.containsKey(currentDirectorID) == false) {
                    directorRatings.put(currentDirectorID, getDirectorRating(currentDirectorID, yahooDescription, items, u1base));
                }
            }
        }

        return directorRatings;
    }

    static double getDirectorRating(int directorID, Instances yahooDescription, Instances items, Instances u1base) {
        double totalRating = 0;
        double totalItems = 0;

        // go through all the movielens items
        for(int i = 0; i < items.numInstances(); i++) {
            Instance currentItem = items.instance(i);
            int currentItemYahooID = (int) currentItem.value(1);
            int currentItemMovieLensID = (int) currentItem.value(0);

            if(currentItem.isMissing(currentItem.attribute(1))) {
                continue; // if item doesn't have a corresponding yahoo ID then just skip it as it is of no use
            }

            int[] directorIDs = getDirectorIDs(currentItemYahooID, yahooDescription);
            if(directorIDs == null) {
                continue; // if the movie doesn't have any director information then just go on to the next one
            }

            // go through all the director IDs to check wherher the director we're looking for is there
            for(int arrayIndex = 0; arrayIndex < directorIDs.length; arrayIndex++) {
                if(arrayIndex == 5) {
                    break;
                }

                int currentDirectorID = directorIDs[arrayIndex];
                // if the movie in question features the director who's rating we're currently finding
                if(currentDirectorID == directorID) {
                    // get the average rating for this movie, returns 0 if no one has rated it (i.e, it is a new item)
                    double averageRating = averageItemRating(currentItemMovieLensID, u1base);
                    if(averageRating > 0) {
                        totalRating += averageRating;
                        totalItems++;
                    }
                    break; // don't need to check rest of the directors once we've found the one we wanted
                }
            }
        }
        if(totalItems > 0) {
            return (totalRating/totalItems);
        } else {
            return 0; // the training matrix does not ensure that each item has ratings, so must account for no ratings for an item
        }
    }

    static int[] getDirectorIDs(int yahooID, Instances yahooDescription) {
        Instance yahooInstance = getInstanceByYahooID(yahooID, yahooDescription);
        if(yahooInstance == null) {
            return null; // case where there is no corresponding yahoo information for the yahoo movie ID
        }

        // vertical bar separated list of the directors of the movie
        String directorIDsString = yahooInstance.stringValue(1);

        // split by vertical bar to get array of director IDs for movie
        String[] directorIDsStringSplit = directorIDsString.split("\\|"); // vertical bar need to be escaped

        try {
            Integer.parseInt(directorIDsStringSplit[0]);
        } catch (Exception e) {
            return null; // case where no director information is provided
        }

        int directorIDs[] = new int[directorIDsStringSplit.length];
        for(int arrayIndex = 0; arrayIndex < directorIDs.length; arrayIndex++) {
            directorIDs[arrayIndex] = Integer.parseInt(directorIDsStringSplit[arrayIndex]);
        }

        return directorIDs;
    }

    /************************************************************************************************************************************************/

    static void awardsBot(HashMap <Integer, ArrayList<Integer>> awardsPartition, Instances u1base) throws Exception {
        SparseInstance awardsInstance = new SparseInstance(u1base.numAttributes());
        Set<Integer> hashMapKeys = awardsPartition.keySet();    // get all the partitions from the hashmap in the form of a set

        // skip last attribute as it is user ID
        for(int i = 0; i < awardsInstance.numAttributes()-1; i++) {
            int movieLensItemID = i+1;  // item ID is one more than index

            // go through all the partitions in the hashmap
            for(Integer partition : hashMapKeys) {
                // if the current partition contains the current item
                if(awardsPartition.get(partition).contains(movieLensItemID)) {
                    // set the item rating to be the average rating of the partition
                    awardsInstance.setValue(movieLensItemID-1, partitionRating(awardsPartition.get(partition), u1base));
                    break;  // no need to go through the rest of the partitions since we've found our item
                }
                awardsInstance.setValue(movieLensItemID-1, 0);  // if the item doesn't belong to any partition just set the rating as 0
            }
        }

        awardsInstance.setValue(u1base.numAttributes()-1, u1base.numInstances()+1); // set user ID of award instance to one more than the current largest one in the ratings matrix
        u1base.add(awardsInstance);

        BufferedWriter writer = new BufferedWriter(new FileWriter("u1basewithawards.arff"));
        writer.write(u1base.toString());
        writer.flush();
        writer.close();
    }

    static double partitionRating(ArrayList<Integer> itemIDs, Instances u1base) {
        double totalRating = 0;
        double totalItems = 0;

        for(int i = 0; i < itemIDs.size(); i++) {
            int currentItemID = itemIDs.get(i);
            double averageItemRating = averageItemRating(currentItemID, u1base);
            // include item in partition rating only if it is not a new item
            if(averageItemRating > 0) {
                totalRating += averageItemRating;
                totalItems++;
            }
        }

        if(totalItems > 0) {
            return (totalRating/totalItems);
        } else {
            return 0; // account for case where all items in the partition have not been rated
        }
    }

    static HashMap<Integer, ArrayList<Integer>> awardsPartition(Instances items, Instances yahooDescription, Instances u1base) {
        // hashmap key is partition while value is the item IDs that belong to that partition
        HashMap<Integer, ArrayList<Integer>> awardsParititon = new HashMap<Integer, ArrayList<Integer>>();

        // go through all the movielens items
        for(int i = 0; i < items.numInstances(); i++) {
            Instance currentItem = items.instance(i);
            int currentItemYahooID = (int) currentItem.value(1);

            if(currentItem.isMissing(currentItem.attribute(1))) {
                continue; // if item doesn't have a corresponding yahoo ID then just skip it as it is of no use
            }

            int awardsWon = getAwardsWon(currentItemYahooID, yahooDescription);
            int awardsNominated = getAwardsNominated(currentItemYahooID, yahooDescription);

            // we only do partition computation if information of both awards won and nominated for is present
            if((awardsNominated == -1) || (awardsWon == -1)) {
                continue;
            }

            // using formula given in the paper
            int partition = (int) ((awardsWon + 0.5*awardsNominated)/3);

            // if the hashmap already contains the partition we just append the current item ID in its arraylist
            if(awardsParititon.containsKey(partition)) {
                awardsParititon.get(partition).add(i+1); // item ID is one more than the index
            } else {
                // if hashmap does not contain partition, we create it and add an arraylist with the current item ID to it
                ArrayList<Integer> itemIDs = new ArrayList<Integer>();
                itemIDs.add(i+1);
                awardsParititon.put(partition, itemIDs);
            }
        }

        return awardsParititon;
    }

    static int getAwardsWon(int yahooID, Instances yahooDescription) {
        Instance yahooInstance = getInstanceByYahooID(yahooID, yahooDescription);
        if(yahooInstance == null) {
            return -1; // case where there is no corresponding yahoo information for the yahoo movie ID
        }

        int awardsWon = 0;
        String awardsStringValue = yahooInstance.stringValue(5);

        try {
            awardsWon = Integer.parseInt(awardsStringValue);
        } catch (Exception e) {
            return -1; // case where no award information is provided for the movie
        }

        return awardsWon;
    }

    static int getAwardsNominated(int yahooID, Instances yahooDescription) {
        Instance yahooInstance = getInstanceByYahooID(yahooID, yahooDescription);
        if(yahooInstance == null) {
            return -1; // case where there is no corresponding yahoo information for the yahoo movie ID
        }

        int awardsNominated = 0;
        String awardsStringValue = yahooInstance.stringValue(6);

        try {
            awardsNominated = Integer.parseInt(awardsStringValue);
        } catch (Exception e) {
            return -1; // case where no nomination informaton is provided for the movie
        }

        return awardsNominated;
    }

    /************************************************************************************************************************************************/

    static void criticBot(Instances items, Instances yahooDescription, Instances u1base) throws Exception {
        SparseInstance criticInstance = new SparseInstance(u1base.numAttributes());

        // ignoring last attribute as it is user ID
        for(int i = 0; i < criticInstance.numAttributes()-1; i++) {
            int currentItemMovieLensItemID = i+1; // item ID is one more than index
            Instance currentItem = items.instance(currentItemMovieLensItemID-1);
            int currentItemYahooID = (int) currentItem.value(1);

            if(currentItem.isMissing(currentItem.attribute(1))) {
                criticInstance.setValue(i, 0); // if item doesn't have a corresponding yahoo ID then just skip it as it is of no use
                continue;
            }

            int criticRating = getCriticRating(currentItemYahooID, yahooDescription);
            // use critic rating only if it is given, otherwise set field to 0
            if(criticRating > 0) {
                criticInstance.setValue(i, criticRating);
            } else {
                criticInstance.setValue(i, 0);
            }
        }
        criticInstance.setValue(u1base.numAttributes()-1, u1base.numInstances()+1);   // set user ID of instance to one more than previous one in matrix
        u1base.add(criticInstance);

        BufferedWriter writer = new BufferedWriter(new FileWriter("u1basewithcritic.arff"));
        writer.write(u1base.toString());
        writer.flush();
        writer.close();
    }

    static int getCriticRating(int yahooID, Instances yahooDescription) {
        Instance yahooInstance = getInstanceByYahooID(yahooID, yahooDescription);
        if(yahooInstance == null) {
            return 0; // case where there is no corresponding yahoo information for the yahoo movie ID
        }

        int criticRating = 0;
        String criticRatingStringValue = yahooInstance.stringValue(3);

        try {
            criticRating = yahooToMovieLensRating(Double.parseDouble(criticRatingStringValue));
        } catch (Exception e) {
            return 0; // case where no critic rating information is provided for the movie
        }

        return criticRating;
    }

    static int yahooToMovieLensRating(double movieLensRating) {

        int rating = 0;

        if((movieLensRating <= 13) && (movieLensRating >= 10.5)) {
            rating = 5;
        } else if((movieLensRating <= 10.5) && (movieLensRating >= 7.5)) {
            rating = 4;
        } else if((movieLensRating <= 7.5) && (movieLensRating >= 4.5)) {
            rating = 3;
        } else if((movieLensRating <= 4.5) && (movieLensRating >= 1.5)) {
            rating = 2;
        } else if(movieLensRating < 1.5) {
            rating = 1;
        }

        return rating;
    }

    /************************************************************************************************************************************************/

    // indexes : action - 7, adventure - 8, animation - 9, childrens - 10, comedy - 11, crime - 12, documentary - 13, drama - 14, fantasy - 15, film_noir - 16
    // horror - 17, musical - 18, mystery - 19, romance - 20, scifi - 21, thriller - 22, war - 23, western - 24

    static void genreBot(HashMap<Integer, Double> genreRatings, Instances items, Instances u1base) throws Exception {

        SparseInstance genreInstance = new SparseInstance(u1base.numAttributes());  // new instance to store ratings of genreBot

        for(int i = 0; i < u1base.numAttributes()-1; i++) {
            int itemID = i+1;   // item ID is one more than attribute index

            double totalRating = 0;
            double totalItems = 0;
            Instance itemInstance = items.instance(itemID-1);   // get corresponding item from item information matrix

            // go through each genre and check which ones the movie belongs to
            for(int genreIndex = 7; genreIndex < items.numAttributes(); genreIndex++) {
                // if the movie belongs to the genre, get the genre rating
                if(itemInstance.value(genreIndex) == 1) {
                    double genreRating = genreRatings.get(genreIndex);
                    // we ignore the genre rating if it is 0, i.e, no one has rated the corresponding movies
                    if (genreRating > 0) {
                        totalRating += genreRating;
                        totalItems++;
                    }
                }
            }

            if(totalItems > 0) {
                genreInstance.setValue(i, (totalRating/totalItems));
            } else {
                genreInstance.setValue(i, 0);   // accounting for case where no one has rated movies of that genre
            }
        }

        genreInstance.setValue(u1base.numAttributes()-1, u1base.numInstances()+1);
        u1base.add(genreInstance);

        BufferedWriter writer = new BufferedWriter(new FileWriter("u1basewithgenre.arff"));
        writer.write(u1base.toString());
        writer.flush();
        writer.close();
    }

    static HashMap<Integer, Double> genreRatings(Instances items, Instances u1base) {
        // hashmap to store genre ratings
        HashMap<Integer, Double> genreRatings = new HashMap<Integer, Double>();
        // go through each genre, calculate rating for each one and store it in hashmap
        for(int i = 7; i < items.numAttributes(); i++) {
            genreRatings.put(i, getGenreRating(items, u1base, i));
        }

        return genreRatings;
    }

    static double getGenreRating(Instances items, Instances u1base, int genreIndex) {

        double totalRating = 0;
        double totalItems = 0;

        // go through all the movielens items
        for(int i = 0; i < items.numInstances(); i++) {
            Instance currentInstance = items.instance(i);
            // check if current item belongs to the genre we're getting the rating for
            if(currentInstance.value(genreIndex) == 1) {
                int itemID = (int) currentInstance.value(0);
                // if it does, get the average rating for this item
                double averageRating = averageItemRating(itemID, u1base);
                // ignore the item if it has no ratings, i.e, it is a new item
                if(averageRating > 0) {
                    totalRating += averageRating;
                    totalItems++;
                }
            }
        }

        if(totalItems > 0) {
            return (totalRating/totalItems);
        } else {
            return 0;   // accounting for case where all movies of this genre have no ratings
        }
    }

    /************************************************************************************************************************************************/

    public static double predictedRating(int userID, int itemID, Instances userRatingTrainingMatrix, HashMap<Integer, double[]> newItemSimilarityMatrix) {
        double numerator = 0.0;
        double denominator = 0.0;
        double similarity;
        double ratedItem_rating;
        double finalRating;

        Instance user = userRatingTrainingMatrix.instance(userID-1); // as noted above, this user ID is 1-indexed while instances field is 0-indexed

        // we skip the final attribute since it's the user ID
        for(int i = 0; i < userRatingTrainingMatrix.numAttributes()-1; i++) {

            if((itemID-1) == i) {
                continue;   // don't want to include the item itself when approximating score
            }

            int item2_ID = i+1; // the ID of the second item is one more than its attribute index

            similarity = newItemSimilarityMatrix.get(itemID)[item2_ID-1]; // gets row of similarities to other items from hashmap by item ID, after that Item IDs 1-indexed and array row fields 0-indexed

            // if similarity between the two items exists and the user has rated this other item
            // similarity threshold seems to greatly reduce the MAE
            if((Double.isNaN(similarity) == false) && (user.value(item2_ID - 1) > 0) && (similarity > 0)) {

                ratedItem_rating = normalize(user.value(item2_ID - 1));
                numerator = numerator + (similarity * ratedItem_rating);
                denominator = denominator + Math.abs(similarity);
            }
        }


        if((numerator > 0 == false) && (denominator > 0 == false)) {
            System.out.println("AVERAGE IS BEING USED");
            return userAverage(user);
        }

        finalRating = numerator/denominator;
        return denormalize(finalRating);
    }

    public static double normalize(double rating) {
        double min_rating = 1.0;
        double max_rating = 5.0;
        double normalized_rating;

        normalized_rating = (2*(rating - min_rating) - (max_rating- min_rating))/(max_rating - min_rating);

        return normalized_rating;
    }

    public static double denormalize(double rating) {
        double min_rating = 1.0;
        double max_rating = 5.0;
        double denormalized_rating;

        denormalized_rating = ((rating + 1) * (max_rating - min_rating))/2 + min_rating;

        return denormalized_rating;
    }

    public static HashMap<Integer, double[]> createNewItemSimilarityMatrix(ArrayList<Integer> newItems, Instances userRatingTrainingMatrix) {
        HashMap<Integer, double[]> newItemSimilarityMatrix = new HashMap<Integer, double[]>();
        double[] similarityRow;

        for(int i = 0; i < newItems.size(); i++) {
            int newItemID = newItems.get(i);
            similarityRow = new double[userRatingTrainingMatrix.numAttributes()-1]; // attributes - 1 because we don't want to include user ID
            for(int j = 0; j < similarityRow.length; j++) {
                similarityRow[j] = itemSimilarity(newItemID, j+1, userRatingTrainingMatrix); // item ID is 1-indexed while attribute numbers are 0-indexed
            }

            newItemSimilarityMatrix.put(newItemID, similarityRow);
        }

        return newItemSimilarityMatrix;
    }

    public static double itemSimilarity(int item1_ID, int item2_ID, Instances userRatingTrainingMatrix) {
        List<Instance> commonUsers = commonUsers(item1_ID, item2_ID, userRatingTrainingMatrix); // list of users who have rated both these items
        double numerator = 0.0;
        double denominator1_noRoot = 0.0;
        double denominator2_noRoot = 0.0;
        double denominator1_root;
        double denominator2_root;
        double item1_rating;
        double item2_rating;

        // refer to pg. 17 of "http://guidetodatamining.com/guide/ch3/DataMining-ch3.pdf" for algorithm

        for(Instance user : commonUsers) {
            item1_rating = user.value(item1_ID-1);  // item ID 1 indexed, attributes 0-indexed
            item2_rating = user.value(item2_ID-1);

            numerator = numerator + ((item1_rating - userAverage(user)) * (item2_rating - userAverage(user)));
            denominator1_noRoot = denominator1_noRoot + Math.pow((item1_rating - userAverage(user)),2);
            denominator2_noRoot = denominator2_noRoot + Math.pow((item2_rating - userAverage(user)),2);
        }

        denominator1_root = Math.sqrt(denominator1_noRoot);
        denominator2_root = Math.sqrt(denominator2_noRoot);

        return ((numerator)/(denominator1_root * denominator2_root));
    }

    public static List<Instance> commonUsers(int item1_ID, int item2_ID, Instances userRatingTrainingMatrix) {

        List<Instance> commonUsers = new LinkedList<Instance>();
        Instance currentUser;
        double item1_value;
        double item2_value;

        for(int i = 0; i < userRatingTrainingMatrix.numInstances(); i++) {
            currentUser = userRatingTrainingMatrix.instance(i);
            item1_value = currentUser.value(item1_ID-1); // item ID is 1-indexed while attribute fields are 0-indexed
            item2_value = currentUser.value(item2_ID-1);
            if((item1_value > 0.0) & (item2_value > 0.0)) {
                commonUsers.add(currentUser);   // add to list if current user has rated both item 1 and 2
            }
        }

        return commonUsers;
    }

    public static double userAverage(Instance user) {
        double itemsRated = 0.0;
        double sum = 0.0;
        double item_rating;

        // we exclude the last attribute since that is the user ID and not a rating
        for(int i = 0; i < user.numAttributes()-1; i++) {
            item_rating = user.value(i);
            if(item_rating > 0.0) {
                itemsRated = itemsRated + 1;
                sum = sum + item_rating;
            }
        }

        return (sum/itemsRated);
    }

    // this method gives the list of items that we are considering as new items and also sets the ratings for all these items to 0 to simulate the fact that it's a new item
    static ArrayList<Integer> newItems(Instances sortedMappedUsers, Instances userRatingTrainingMatrix) {
        ArrayList<Integer> newItems = new ArrayList<Integer>();

        int ID = 1;
        int total = 0;  // variable to store the number of ratings for a particular item

        for(int i = 0; i < sortedMappedUsers.numInstances(); i++) {
            if(ID == sortedMappedUsers.instance(i).value(1)) {
                total++;
                // if it is the last user entry we're looking at we must definitely execute the below code
                if(i != sortedMappedUsers.numInstances()-1) {
                    continue;   // this ensures that we don't get to bottom part till counting has been completed
                }
            }

            if(total > 350) {
                newItems.add(ID);
                // go through all rows and set the ratings of this item to 0 (essentially making it behave like a new item)
                // last 5 instances are the bot ratings which we should leave untouched
                for(int j = 0; j < userRatingTrainingMatrix.numInstances()-5; j++) {
                    // ID-1 because ID is 1-indexed while the attributes are 0-indexed
                    userRatingTrainingMatrix.instance(j).setValue(ID-1, 0);
                }
            }

            total = 1;  // breaking out of initial if statement means that we have moved on to new ID, so need to set total as 1 to count the current new item
            ID++;
        }

        return newItems;
    }

    /************************************************************************************************************************************************/
    // binary search makes this a LOT faster
    static Instance getInstanceByYahooID(int yahooID, Instances yahooDescription) {
        int high = yahooDescription.numInstances()-1;
        int low = 0;
        int medium;

        while(high >= low) {
            medium = (high+low)/2;
            Instance mediumInstance = yahooDescription.instance(medium);
            int mediumValue = (int) mediumInstance.value(0);

            if(mediumValue == yahooID) {
                return mediumInstance;
            } else if(yahooID > mediumValue) {
                low = medium + 1;
            } else if(yahooID < mediumValue) {
                high = medium-1;
            }
        }

        return null;
    }

    static double averageItemRating(int itemID, Instances u1base) {
        double totalRating = 0;
        double totalItems = 0;

        for(int i = 0; i < u1base.numInstances(); i++) {
            Instance currentInstance = u1base.instance(i);
            double rating = currentInstance.value(itemID-1); // item ID is one indexed while attributes are 0-indexed

            if(rating > 0) {
                totalRating += rating;
                totalItems++;
            }
        }

        if(totalItems > 0) {
            return (totalRating / totalItems);
        } else {
            return 0;
        }
    }
}
