import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.CSVLoader;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;

// go through both the movielens and yahoo datasets and assign yahoo IDs to movielens movies

public class YahooToMovieLens {

    public static void main(String[] args) throws Exception {

        Instances movieLensItems = null;

        try {
            File file = new File("./u.item");
            CSVLoader csvLoader = new CSVLoader();
            csvLoader.setSource(file);
            csvLoader.setFieldSeparator("|");
            movieLensItems = csvLoader.getDataSet();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        Instances yahooItems = null;

        try {
            File file = new File("./mapping.txt");
            CSVLoader csvLoader = new CSVLoader();
            csvLoader.setSource(file);
            csvLoader.setFieldSeparator("|");
            yahooItems = csvLoader.getDataSet();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        Instances combinedIDs = new Instances(movieLensItems);
        combinedIDs.insertAttributeAt(new Attribute("yahooID"), 1);

        ArrayList<Integer> noMapping = new ArrayList<Integer>();

        for(int i = 0; i < movieLensItems.numInstances(); i++) {
            Instance currentMovieLensInstance = movieLensItems.instance(i);
            String movieLensTitle = currentMovieLensInstance.stringValue(1);

            boolean didItBreak = false;

            for(int j = 0; j < yahooItems.numInstances(); j++) {
                Instance currentYahooInstance = yahooItems.instance(j);
                String yahooTitle = currentYahooInstance.stringValue(1);

                if(movieLensTitle.equals(yahooTitle)) {
                    combinedIDs.instance(i).setValue(1, currentYahooInstance.value(0));
                    didItBreak = true;
                    break;
                }
                didItBreak = false;
            }

            if(didItBreak == false) {
                noMapping.add((int) currentMovieLensInstance.value(0));
            }
        }

        for(int i = 0; i < noMapping.size(); i++) {
            System.out.print(noMapping.get(i) + ",");
        }

        BufferedWriter writer = new BufferedWriter(new FileWriter("combinedIDs.arff"));
        writer.write(combinedIDs.toString());
        writer.flush();
        writer.close();

    }
}
