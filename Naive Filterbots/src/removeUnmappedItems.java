import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ArffLoader;
import weka.core.converters.CSVLoader;
import weka.core.converters.CSVSaver;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Varun on 26/12/2014.
 */

// go through rating matrix and u_sorted and remove items that don't have a corresponding yahoo ID

public class removeUnmappedItems {

    public static void main(String[] args) throws Exception {
        Instances u1base = null;

        try {
            File file = new File("./u1base.arff");
            ArffLoader arffLoader = new ArffLoader();
            arffLoader.setSource(file);
            u1base = arffLoader.getDataSet();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        Instances uSorted = null;

        try {
            File file = new File("./u_sorted.csv");
            CSVLoader csvLoader = new CSVLoader();
            csvLoader.setSource(file);
            uSorted = csvLoader.getDataSet();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        Remove remove = new Remove();
        remove.setAttributeIndices("14,18,19,20,24,42,44,45,55,59,60,61,84,128,139,141,151,154,163,168,169,170,172,177,181,189,201,211,214,217,243,251,253,267,280,314,379,408,424,455,462,474,488,495,510,512,529,559,561,563,567,580,600,634,643,646,665,679,694,701,707,719,730,753,774,789,811,830,839,904,906,919,921,923,927,947,961,963,1005,1018,1019,1104,1115,1128,1129,1143,1156,1158,1201,1230,1240,1252,1286,1296,1318,1322,1346,1348,1358,1359,1373,1402,1403,1412,1420,1427,1472,1494,1495,1501,1548,1563,1573,1586,1592,1626,1631,1633,1634,1635,1639,1640,1645,1650,1682");
        remove.setInputFormat(u1base);
        u1base = Filter.useFilter(u1base, remove);

        BufferedWriter writer = new BufferedWriter(new FileWriter("u1baseOnlyMapped.arff"));
        writer.write(u1base.toString());
        writer.flush();
        writer.close();

        int[] nonMapped = {14,18,19,20,24,42,44,45,55,59,60,61,84,128,139,141,151,154,163,168,169,170,172,177,181,189,201,211,214,217,243,251,253,267,280,314,379,408,424,455,462,474,488,495,510,512,529,559,561,563,567,580,600,634,643,646,665,679,694,701,707,719,730,753,774,789,811,830,839,904,906,919,921,923,927,947,961,963,1005,1018,1019,1104,1115,1128,1129,1143,1156,1158,1201,1230,1240,1252,1286,1296,1318,1322,1346,1348,1358,1359,1373,1402,1403,1412,1420,1427,1472,1494,1495,1501,1548,1563,1573,1586,1592,1626,1631,1633,1634,1635,1639,1640,1645,1650,1682};
        ArrayList<Integer> nonMappedList = new ArrayList<Integer>();

        for (int index = 0; index < nonMapped.length; index++) {
            nonMappedList.add(nonMapped[index]);
        }

        for(int i = uSorted.numInstances()-1; i >= 0; i--) {
            Instance currentInstance = uSorted.instance(i);

            if(nonMappedList.contains((int) currentInstance.value(1))) {
                System.out.println("deleting " + currentInstance.value(1));
                uSorted.delete(i);
            }
        }

        CSVSaver csvSaver = new CSVSaver();
        csvSaver.setInstances(uSorted);
        csvSaver.setFile(new File("./u_sortedOnlyMapped.csv"));
        csvSaver.writeBatch();

/*        writer = new BufferedWriter(new FileWriter("u_sortedOnlyMapped.arff"));
        writer.write(u1base.toString());
        writer.flush();
        writer.close();*/
    }

}
