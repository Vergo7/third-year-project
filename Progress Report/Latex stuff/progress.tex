\documentclass[10pt,a4paper]{report}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{cite}
\usepackage{url}
\usepackage[left=2cm,right=2cm,top=2cm,bottom=2cm]{geometry}
\title{Collaborative Filtering - the New Item problem}
\usepackage{pdfpages}
\author{Varun Golani}
\begin{document}
\maketitle

\tableofcontents

\chapter*{Progress Report}
\stepcounter{chapter}

\section{Introduction}

Collaborative filtering is a common technique used in recommender systems. Recommendations are provided to users on the basis of finding users most similar to the one under consideration or finding items that are most similar to the ones that the user has rated highly. These two approaches are known as user-based and item-based collaborative filtering respectively. 

One of the primary issues of this technique is what is known as the ``cold-start'' problem and can apply to both users and items. The user cold-start refers to the situation where a new user in the system must first rate multiple items before they can be provided with recommendations. The item cold-start (also referred to as the new-item problem) refers to the situation where an item that has been recently introduced into the system must garner initial user ratings before the algorithm can select this item to be recommended to other users. 

As there is no definite, complete solution that deals with the new-item problem this project analyses some of the partial solutions that have been proposed and evaluates their effectiveness across various datasets.



\section{Background Research}

In order to gain familiarity with the new-item problem and collaborative filtering in general, a wide variety of academic papers were perused. These include ``Collaborative Filtering Recommender Systems''~\cite{schafer} which introduces the fundamental concepts of collaborative filtering and the primary metrics used in the evaluation of such algorithms. ``A Comparative Study of Collaborative Filtering Algorithms''~\cite{comparative} and ``Evaluating Collaborative Filtering Recommender Systems''~\cite{evaluation} provide an overview of the various approaches to standard collaborative filtering, compare and contrast them and describe how they perform in different environments. These papers provided an initial idea of the types of implementation approaches and which of these perform better than others given the situation.  

In addition to these papers a copy of Ms. Lucy Davies' 3rd year project in 2010/2011~\cite{lucy} was acquired through the reception at DCS as it too tackled collaborative filtering. Reading through this was useful as it provided a general idea of how to implement basic collaborative filtering and the type of work that would be involved in the entirety of the project.  

Various partial solutions to the new-item problem were looked at, including a market-based approach~\cite{griffiths} and utilisation of a probabilistic model~\cite{kim1}. The complete list of solutions considered is given in the specification.


\section{Overview and Progress}

\subsection{Objective Progress}

These were the original objectives for the work in term 1 that were outlined in the Project Specification document 

\begin{enumerate}

\item Background reading 
\item Basic collaborative filtering implementation and evaluation
\item Implementation of 1st partial solution
\item Implementation of 2nd partial solution 

\end{enumerate}

The progress made in each of these objectives is given below

\begin{enumerate}

\item As described in the Background Reading section in this document a significant amount of time was spent exploring various academic papers and online resources to gain familiarity with collaborative filtering and the various proposed partial solutions to the new item problem. While the majority of the reading was done within the two weeks allocated for this task in the specification document this has actually been an ongoing process throughout the term as greater value was extracted from revisiting these partial solutions after gaining a thorough understanding of how basic collaborative filtering works.

\item Implementation of basic collaborative filtering can be divided into two parts
\begin{enumerate}

\item \textbf{User-based collaborative filtering} - This approach essentially finds users in the system that are most similar to the user we want to provide recommendations for. Once we have identified this set of similar users we recommend items that these similar users have rated highly. This was implemented using the method described in ``Instant Weka How-to''~\cite{wekabook} where the most similar users to the one under consideration are found using the nearest neighbour search in WEKA~\cite{weka}. One week was allotted to this task in the specification and it was completed on time.

\item \textbf{Item-based collaborative filtering} - Instead of finding the most similar users, in this approach we find the items that are most similar to the ones that the user under consideration has rated highly and recommend those. This was implemented using Adjusted Cosine Similarity as described in chapter 3 of ``A Programmer's Guide to Data Mining''~\cite{datamining}. While the timetable accounted for only one collaborative filtering approach to be implemented, after analysing the partial solutions to be tackled later on it seemed useful to implement the item-based approach too as it is central to some of these solutions. This was completed during the first review and slack time period allocated.

\end{enumerate}

\item The first partial solution to the new-item problem to be implemented was the \textbf{Market-based Approach}~\cite{griffiths}. This approach seeds recommendations for new items by having producers of these items bid to have them presented to the most influential users in the system. These users can then decide whether to rate the items or not. As an incentive, users are paid on an earn-per-rating basis.

The specification allocated two weeks for this task. However the implementation of this algorithm took significantly longer than expected (almost four weeks) primarily due to the fact that the complexity of this task was underestimated. Furthermore, of all the solutions explored this was perhaps the most challenging and unorthodox one. Taking into account these factors the decision to attempt this one first was made as it was expected to be the most time-consuming solution.

\item Due to the implementation of the market-based approach taking longer than expected no work has started on this part yet.

\end{enumerate}

\subsection{Overview}

The project is thus slightly behind schedule for the reason described above. However considering that the market-based approach was relatively more complex than the other ones explored and given that the work initially allocated for the entirety of the Christmas vacations was kept light to enable taking stock of the situation and account for any overflow of work from term 1, the project is in a comfortable state and should be able to follow the original schedule for term 2.

\section{Appraisal}

I am fairly satisfied with the way the project has gone so far. I feel that I was slightly ambitious with my work schedule for term 1 and underestimated the time and effort required to implement the partial solutions, which is primarily why the project is slightly behind schedule. However now that I have actually implemented a solution in its entirety I believe that I am now thoroughly comfortable with the concepts involved and predict that the implementation of future solutions should go a lot more smoothly. My initial decision of keeping the workload during the Christmas vacations low enabled me to spend a sufficient amount of time understanding and implementing the market-based solution and has given me enough room to get back on schedule for term 2.
 
One important thing I have learnt during the course of this term is the importance of keeping slack time for the tasks involved. There have been various instances where something unexpected came up preventing me from working on the project in accordance with my weekly schedule, and the presence of this slack time gave me the breathing space required to make the necessary adjustments. This gives me the confidence that originally keeping slack times for the tasks in term 2 was indeed the correct decision.


\section{Revised Timetable}

As described in the section above the Christmas vacations will be used to catch up with the backlog of term 1. This means that the timetable for term 2 remains the same as what was provided in the Gantt chart in the specification. Now that significant progress has been made in the project the future stages can be described in greater detail -

\begin{center}
	\begin{tabular}{ | l | l | p{2.5cm} | p{5cm} |}
	\hline	
	Start Date & End Date & Objective & Description \\ \hline
	8th December & 4th January & Algorithms evaluation and comparison & All implementations till now have been tested only on the MovieLens dataset~\cite{movielens}. Once they have been adjusted to use the other datasets they can be evaluated and compared against each other. \\ \hline
	8th December & 4th January & Reading on other algorithms and datasets & Further research will be done to see whether there are other datasets or solutions that should be considered apart from the ones listed in the specification.  \\ \hline
	8th December & 4th January & 2nd algorithm implementation & As mentioned before the second solution will be implemented during the vacations to get back on schedule for term 2. The specific solution to be implemented will be decided on before the start of the vacations.  \\ \hline
	5th January & 18th January & Additional algorithm implementation/slack time & As the original specification mentioned that a minimum of 2 solutions will be implemented, this is an optional objective to implement another solution if time permits. Otherwise this will be used as slack time to catch up with any backlog.  \\ \hline
	5th January & 15th February & Summarise findings and explore extensions & Once the algorithms have been implemented and evaluated, the process of collecting and drawing conclusions from the results will begin in preparation for the final report. This time will also be used to explore possible extensions to the existing solutions to see whether performance can be improved. \\ \hline
	16th February & N.A & Presentation preparation & Once the project has been completed and the conclusions have been collated, preparation for the presentation will begin. \\ \hline
	\end{tabular}
\end{center}

\section{Ethical Considerations}

All the datasets used in this project consist of user data available in the public domain and are anonymised. In addition, they are freely available for research use when acknowledged in an appropriate manner.  

\section{Project Management}

As mentioned in the specification an agile approach was adopted for this project. This is primarily because the partial solutions to the new-item problem build upon the concepts and methods used in basic user and item based collaborative filtering which fits perfectly with the incremental nature of agile and ensures steady progress. In addition to the methodology, the project was kept on track by breaking down each objective into smaller components and following a fixed weekly schedule for work as far as possible. Furthermore I have had scheduled weekly meetings with my supervisor throughout the term to discuss progress made and chart out the plan and objectives for the coming week. 

In accordance with the specification a private BitBucket repository is being used to back up all project data and keep track of the work that has been done till now through the commits.

\bibliography{mybib}
\bibliographystyle{plain}

\section{Appendix - Project Specification}

\includepdf[pages={2-4}]{specification.pdf}

\end{document}
