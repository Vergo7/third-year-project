\documentclass[10pt,a4paper]{report}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{cite}
\usepackage{url}
\usepackage[left=2cm,right=2cm,top=2cm,bottom=2cm]{geometry}
\title{Collaborative Filtering - the New Item problem}
\author{Varun Golani}
\begin{document}
\maketitle

\chapter*{Project Specification}
\stepcounter{chapter}

\section{Problem}

Collaborative filtering is an information filtering technique used by many recommender systems. A collaborative filtering system essentially takes a user's rating of multiple items (rating matrix) and produces predictions or recommendations for this user and one or more items. The term user refers to ``any individual who provides ratings to a system'' while items can consist of ``anything for which a human can provide a rating, such as art, books, CDs, journal articles, or vacation destinations''~\cite{schafer}. The most well-known way of achieving this is the item-based nearest neighbour algorithm, which generates ``predictions based on similarities between items" where ``the prediction for an item should be based on a user’s ratings for similar items''~\cite{schafer}.

The new-item problem refers to the situation where an item has just been introduced into the database. As this item does not have any ratings initially its similarity to other items rated by the users cannot be evaluated and thus this item will not be recommended by the system until it is found manually and rated by one or more users. As there is no definite, complete solution that deals with this problem this project analyses some of the partial solutions that have been proposed and evaluates their effectiveness across various datasets.


\section{Objectives}

This project will analyse partial solutions to the new-item problem in collaborative filtering. These solutions will be evaluated using at least two datasets. Examples of ones that might be used are as follows 

\subsection{Datasets}

\begin{enumerate}
  \item \textbf{MovieLens} (Movie rating datasets) - MovieLens is a movie recommendation website that gives users personalised recommendations based on their ratings of movies that they have already seen. ``GroupLens Research has collected and made available rating data sets from the MovieLens web site.  The data sets were collected over various periods of time, depending on the size of the set''~\cite{movielens}.
  \item \textbf{Last.fm} (Music rating datasets) - Last.fm is a music recommendation service that uses a scrobbler to provide users with recommendations. ``This dataset contains (user, artist-mbid, artist-name, total-plays) tuples collected from Last.fm API, using the user.getTopArtists() method''~\cite{lastfm}. 
  \item \textbf{Jester} (Joke rating datasets) - Jester uses a collaborative filtering algorithm called Eigentaste to recommend jokes to users~\cite{jester}.
  \item \textbf{BookCrossing} (Book rating dataset) - BookCrossing is a website that allows users to track and rate books that they have read. The dataset was ``collected by Cai-Nicolas Ziegler in a 4-week crawl (August / September 2004) from the Book-Crossing community''~\cite{book}. 

\end{enumerate}

\subsection{Partial solutions}

There are numerous partial solutions that have been proposed to improve the performance of collaborative filtering algorithms in cold start situations. This project will evaluate and compare at least two of them among the following

\begin{enumerate}
  \item \textbf{A Market-based Approach to Address the New Item Problem} - This algorithm adopts a market-based approach for seeding recommendations for new items, having publishers bid to have items presented to the most influential users who rate these items on an earn-per-rating basis~\cite{griffiths}. 
  \item \textbf{Methods and Metrics for Cold-Start Recommendations} - This algorithm adopts a hybrid approach by ``combining content and collaborative data under a single probabilistic framework''~\cite{schein}.
  \item \textbf{Probabilistic Model Estimation for Collaborative Filtering Based on Items Attributes} - This approach also utilises a probabilistic model where ``items are classified into groups and predictions are made for users considering the Gaussian distribution of user ratings'' to solve the problems of user bias, non-transitive association and new item problem in collaborative recommender systems~\cite{kim1}.
  \item \textbf{Naive Filterbots for Robust Cold-Start Recommendations} - This approach utlisies simple non-personalized recommendations by making use of ``filterbots, or surrogate users that rate items based only on user or item attributes''~\cite{park}.
  \item \textbf{Content-Boosted Collaborative Filtering for Improved Recommendations} - This algorithm uses a hybrid of collaborative filtering and content-based methods by using a content-based predictor to enhance existing user data and then provide personalised suggestions through collaborative filtering~\cite{melville}. 
  \item \textbf{An Approach for Combining Content-based and Collaborative Filters} - This approach uses a clustering technique to obtain group rating information that ``provides a way to introduce content information into collaborative recommendation and solves the cold start problem''~\cite{kim2}. 
\end{enumerate}

The main objective of the project is to explore the new-item problem in depth and ascertain whether the currently available approaches are equally effective across datasets with different characteristics. The project will also compare and contrast the various options against each other and seek to establish whether it is possible to improve on them by either combining approaches or developing something new entirely. 

\section{Methodology}

An agile approach will be utilised in this project as it will involve incremental development of the various partial solutions described above and will need to be flexible enough to accommodate changes in the order and content of stages.  The primary metric for evaluating the effectiveness of each solution will be the accuracy of the system's predictions. Accuracy in this context is computed using mean absolute error, which is defined as ``the average absolute difference between the predicted rating and the actual rating given by a user''~\cite{schafer}. Other metrics such as novelty, coverage and confidence may also be taken into consideration when directly comparing approaches. 

\section{Resources}

The majority of the project work will be done on a local machine. In addition the Computer Science department's servers might be used during evaluation of the implemented algorithms. A private BitBucket repository is being utilised to back up all the project work. 

\section{Ethical Considerations}

All the datasets used in this project consist of user data available in the public domain and are anonymised. In addition, they are freely available for research use when acknowledged in an appropriate manner.  

\section{Timetable}

The rough timetable for the overall project is given below in the form of a Gantt chart

\begin{figure}[hp]
\includegraphics[width=1\textwidth, height=15\textheight,keepaspectratio]{Project.png}
\end{figure}

\bibliography{mybib}
\bibliographystyle{plain}


\end{document}
