Code

Each algorithm implementation is assigned a separate folder, consisting of all the code, required libraries and datasets used.  
Distinction is made between the MovieLens and Hummingbird implementations.

The folder names indicate which algorithm they contain. Apart from the Clustering method being referred to as the 'Hybrid' method, the 
nomenclature is consistent with the report. 

Results 

The detailed results of running each algorithm is present in the respective folders in documents titled 'Results.txt'. They contain the
MAEs recorded over all parameters. 

Running the project 

Each folder is essentially a separate, self-sufficient IntelliJ project included with all required libraries and other dependencies. Thus
any algorithm can be run by opening the required project in IntelliJ and executing the main class. 

Documents 

Also included on the disk are the Project Specification and Progress Report documents. These are in the folder titled 'Documents'