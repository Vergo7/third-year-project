Hummingbird rating dataset consists of 5845 items and 1000 users, both have incremental IDs starting from 1

The raw data had over 7 million entries and thus needed to be filtered. First, all entries with 0 rating were removed as they would 
not contribute to the algorithms. After that all users with more than 20 ratings were identified, and out of these 1000 random users
were selected to be included in the final output. Entries from other users were deleted.   

The user IDs in the raw data were random so these were converted to incremental IDs by first sorting
the entries and then going through them one at a time. The ratings for each entry were also multiplied by 2 to get them on a scale of
1-10

The WEKA GUI was used to extract all the item slugs, and then these were used to get the genre information for each item from the
hummingbird API

Finally the slugs in the ratings file were replaced by the item IDs assigned in the item file to generate the hummingbirdFullRatings file 