import weka.core.Instances;
import weka.core.converters.ArffSaver;
import weka.core.converters.CSVSaver;
import weka.core.converters.ConverterUtils;

import java.io.File;
import java.util.Random;

/**
 * Created by Varun on 22/01/2015.
 */
public class TrainingTestDataGenerate {
    public static void main(String[] args) {
        Instances dataset = null;
        try {
            dataset = ConverterUtils.DataSource.read("hummingbirdFullRatings.arff");
        } catch (Exception e) {
            e.printStackTrace();
        }

        /** Generate a training (80%) a test (20%) dataset **/
        Instances trainingData = null, testData = null;

        /** Randomize dataset with known seed for repeatable experiments **/
        dataset.randomize(new Random(1));

        /** Choose 80% of the data as training data **/
        double percentTrain = 0.8f;
        int trainSize = (int)(dataset.numInstances() * percentTrain);
        trainingData = new Instances(dataset, 0, trainSize);
        trainingData.setRelationName("Hummingbird-TrainData");
        trainingData.sort(0);
        // Method to write instances to file
        writeDataToCSV("hummingbirdBase1.csv", trainingData);

        /** Choose 20% of the data as test data **/
        int testSize = dataset.numInstances()-trainSize;
        testData = new Instances(dataset, trainSize, testSize);
        testData.setRelationName("Hummingbird-TestData");
        testData.sort(0);
        writeDataToCSV("hummingbirdTest1.csv", testData);
        System.out.println(trainingData.numInstances()+" + "+testData.numInstances()+" = "+dataset.numInstances());
    }

    public static void writeDataToCSV(String filename, Instances instances)
    {
        try{
            CSVSaver saver = new CSVSaver();
            saver.setInstances(instances);
            saver.setFile(new File(filename));
            // necessary in 3.5.4 and later
            saver.writeBatch();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
