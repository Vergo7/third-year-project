import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import jdk.nashorn.api.scripting.JSObject;
import org.json.JSONArray;
import org.json.JSONObject;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.HashMap;

/**
 * Created by Varun on 18/01/2015.
 */
public class genreData {
    public static void main(String[] args) throws Exception {
        Instances data = null;
        try {
            data = ConverterUtils.DataSource.read("items.csv");

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        Instances genreDescription; // database to store the items with genres
        FastVector atts = new FastVector();
        HashMap<String,Integer> genreIndexMap = new HashMap<String, Integer>(); // maps genre names to corresponding index in database

        atts.addElement(new Attribute("itemID"));
        atts.addElement(new Attribute("itemSlug", (FastVector) null));

        // genres taken from Hummingbird's 'explore' page
        atts.addElement(new Attribute("Action")); // index 2
        genreIndexMap.put("Action",2);
        atts.addElement(new Attribute("Adventure"));
        genreIndexMap.put("Adventure", 3);
        atts.addElement(new Attribute("Anime Influenced"));
        genreIndexMap.put("Anime Influenced", 4);
        atts.addElement(new Attribute("Cars"));
        genreIndexMap.put("Cars", 5);
        atts.addElement(new Attribute("Comedy"));
        genreIndexMap.put("Comedy",6);
        atts.addElement(new Attribute("Dementia"));
        genreIndexMap.put("Dementia", 7);
        atts.addElement(new Attribute("Demons"));
        genreIndexMap.put("Demons", 8);
        atts.addElement(new Attribute("Drama"));
        genreIndexMap.put("Drama", 9);
        atts.addElement(new Attribute("Ecchi"));
        genreIndexMap.put("Ecchi",10);
        atts.addElement(new Attribute("Fantasy"));
        genreIndexMap.put("Fantasy",11);
        atts.addElement(new Attribute("Game"));
        genreIndexMap.put("Game",12);
        atts.addElement(new Attribute("Harem"));
        genreIndexMap.put("Harem",13);
        atts.addElement(new Attribute("Hentai"));
        genreIndexMap.put("Hentai",14);
        atts.addElement(new Attribute("Historical"));
        genreIndexMap.put("Historical",15);
        atts.addElement(new Attribute("Horror"));
        genreIndexMap.put("Horror",16);
        atts.addElement(new Attribute("Kids"));
        genreIndexMap.put("Kids",17);
        atts.addElement(new Attribute("Magic"));
        genreIndexMap.put("Magic",18);
        atts.addElement(new Attribute("Martial Arts"));
        genreIndexMap.put("Martial Arts",19);
        atts.addElement(new Attribute("Mecha"));
        genreIndexMap.put("Mecha",20);
        atts.addElement(new Attribute("Military"));
        genreIndexMap.put("Military",21);
        atts.addElement(new Attribute("Music"));
        genreIndexMap.put("Music",22);
        atts.addElement(new Attribute("Mystery"));
        genreIndexMap.put("Mystery",23);
        atts.addElement(new Attribute("Parody"));
        genreIndexMap.put("Parody",24);
        atts.addElement(new Attribute("Police"));
        genreIndexMap.put("Police",25);
        atts.addElement(new Attribute("Psychological"));
        genreIndexMap.put("Psychological",26);
        atts.addElement(new Attribute("Romance"));
        genreIndexMap.put("Romance",27);
        atts.addElement(new Attribute("Samurai"));
        genreIndexMap.put("Samurai",28);
        atts.addElement(new Attribute("School"));
        genreIndexMap.put("School",29);
        atts.addElement(new Attribute("Sci-Fi"));
        genreIndexMap.put("Sci-Fi",30);
        atts.addElement(new Attribute("Shoujo Ai"));
        genreIndexMap.put("Shoujo Ai",31);
        atts.addElement(new Attribute("Shounen Ai"));
        genreIndexMap.put("Shounen Ai",32);
        atts.addElement(new Attribute("Slice of Life"));
        genreIndexMap.put("Slice of Life",33);
        atts.addElement(new Attribute("Space"));
        genreIndexMap.put("Space",34);
        atts.addElement(new Attribute("Sports"));
        genreIndexMap.put("Sports",35);
        atts.addElement(new Attribute("Supernatural"));
        genreIndexMap.put("Supernatural",36);
        atts.addElement(new Attribute("Super Power"));
        genreIndexMap.put("Super Power",37);
        atts.addElement(new Attribute("Thriller"));
        genreIndexMap.put("Thriller",38);
        atts.addElement(new Attribute("Vampire"));
        genreIndexMap.put("Vampire",39);
        atts.addElement(new Attribute("Yaoi"));
        genreIndexMap.put("Yaoi",40);
        atts.addElement(new Attribute("Yuri"));
        genreIndexMap.put("Yuri",41);

        genreDescription = new Instances("MyRelation", atts, 0);

        // go through each item
        for(int i = 0; i < data.numInstances(); i++) {
            String animeSlug = data.instance(i).stringValue(0);

            // These code snippets use an open-source library. http://unirest.io/java
            // accesses Hummingbird API to get JSON response
            HttpResponse<JsonNode> response = Unirest.get("https://hummingbirdv1.p.mashape.com/anime/" + animeSlug)
                    .header("X-Mashape-Key", "lEFJ2pmEd0mshx19CAGhtrLBMoZIp1BczXzjsn1mDQ2JZBAseq")
                    .header("Accept", "application/json")
                    .asJson();

            JSONObject animeData = response.getBody().getObject();
            JSONArray genres = animeData.getJSONArray("genres");

            double[] vals = new double[genreDescription.numAttributes()];

            // goes through each genre that the response returned
            for (int j = 0; j < genres.length(); j++) {
                System.out.println(genres.getJSONObject(j).getString("name"));
                String genreName = genres.getJSONObject(j).getString("name");
                // use map to find index position of genre obtained
                int genreIndex = genreIndexMap.get(genreName);
                // set the corresponding genre field to 1
                vals[genreIndex] = 1;
            }

            vals[0] = i+1;  // ID is one more than index
            vals[1] = genreDescription.attribute("itemSlug").addStringValue(animeSlug);
            genreDescription.add(new Instance(1.0, vals));
        }
        BufferedWriter writer = new BufferedWriter(new FileWriter("itemsWithGenres.arff"));
        writer.write(genreDescription.toString());
        writer.flush();
        writer.close();
    }
}
