import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;

import java.io.BufferedWriter;
import java.io.FileWriter;

/**
 * Created by Varun on 18/01/2015.
 */
public class incrementalUserID {
    public static void main(String[] args) throws Exception {
        Instances data = null;
        try {
            data = ConverterUtils.DataSource.read("vergo_small.csv");

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        int currentIncrementalID = 0;
        int previousID = 0;
        int currentID = 0;

        for(int i = 0; i < data.numInstances(); i++) {
            Instance currentInstance = data.instance(i);
            currentID = (int) currentInstance.value(0); // gets the ID of the current row

            currentInstance.setValue(2, (currentInstance.value(2)*2));  // multiply all ratings by 2 to make them in range 1-10

            // if ID of previous row is same as ID of this one
            if(currentID == previousID) {
                currentInstance.setValue(0, currentIncrementalID);  // set the value of this row to be the current incremental ID
                previousID = currentID; // set previous ID variable to this row's current ID for the next row
            } else {
                currentIncrementalID++; // new bunch of IDs, hence increase incremental value
                previousID = (int) currentInstance.value(0); // save ID of this row in previousID variable for matching with next row
                currentInstance.setValue(0, currentIncrementalID);  // set the ID of this row to be the current incremental ID
            }
        }

        BufferedWriter writer = new BufferedWriter(new FileWriter("incrementalID.arff"));
        writer.write(data.toString());
        writer.flush();
        writer.close();
    }
}
