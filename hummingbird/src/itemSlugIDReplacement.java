import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;

import java.io.BufferedWriter;
import java.io.FileWriter;

/**
 * Created by Varun on 18/01/2015.
 */
public class itemSlugIDReplacement {
    public static void main(String[] args) throws Exception {
        Instances incrementalID = null;
        Instances itemsWithGenres = null;
        try {
            incrementalID = ConverterUtils.DataSource.read("incrementalID.csv");
            itemsWithGenres = ConverterUtils.DataSource.read("itemsWithGenres.arff");
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        FastVector atts = new FastVector();

        atts.addElement(new Attribute("userID"));
        atts.addElement(new Attribute("itemID"));
        atts.addElement(new Attribute("rating"));

        Instances hummingbirdBase = new Instances("MyRelation", atts, 0);; // database to store the items with genres

        double[] vals;

        // go through each rating instance
        for(int i = 0; i < incrementalID.numInstances(); i++) {
            Instance currentInstance = incrementalID.instance(i);
            String currentSlug = currentInstance.stringValue(1);

            // go through each item
            for(int j = 0; j < itemsWithGenres.numInstances(); j++) {
                Instance currentItem = itemsWithGenres.instance(j);
                String currentItemSlug = currentItem.stringValue(1);

                // if current rating slug is equal to the current item slug
                if(currentSlug.equals(currentItemSlug)) {
                    vals = new double[hummingbirdBase.numAttributes()];

                    int userID = (int) currentInstance.value(0);
                    int itemID = (int) currentItem.value(0);
                    int rating = (int) currentInstance.value(2);

                    vals[0] = userID;
                    vals[1] = itemID;
                    vals[2] = rating;
                    hummingbirdBase.add(new Instance(1.0, vals));

                    break;
                }
            }
        }

        BufferedWriter writer = new BufferedWriter(new FileWriter("hummingbirdFullRatings.arff"));
        writer.write(hummingbirdBase.toString());
        writer.flush();
        writer.close();
    }
}
