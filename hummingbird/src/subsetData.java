import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;
import weka.filters.Filter;
import weka.filters.unsupervised.instance.RemoveWithValues;
import weka.filters.unsupervised.instance.ReservoirSample;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;

public class subsetData {

    public static void main(String[] args) throws Exception {

        Instances data = null;
        try {
            data = ConverterUtils.DataSource.read("vergo_without0.csv");

        } catch (Exception ex) {
            ex.printStackTrace();
        }

/*        RemoveWithValues removeWithValues = new RemoveWithValues();
        removeWithValues.setSplitPoint(0.1);
        removeWithValues.setInputFormat(data);

        data = Filter.useFilter(data, removeWithValues);*/

        data.sort(0);
        ArrayList<Integer> usersOverTwentyRatings = new ArrayList<Integer>();

        int ID = (int) data.firstInstance().value(0);
        int total = 0;

        // count the number of ratings made by each user
        for(int i = 0; i < data.numInstances(); i++) {
            Instance currentInstance = data.instance(i);
            if(ID == currentInstance.value(0)) {
                total++;
                // if it is the last user entry we're looking at we must definitely execute the below code
                if(i != data.numInstances()-1) {
                    continue;
                }
            }

            if(total > 20) {
                System.out.println("User " + ID + " has " + total + " ratings");
                usersOverTwentyRatings.add(ID);
            }

            total = 1;
            ID++;
        }

        // randomise the users we pick for the final selection
        Collections.shuffle(usersOverTwentyRatings);
        ArrayList<Integer> finalUsers = new ArrayList<Integer>();

        for(int i = 0; i < 1000; i++) {
            System.out.println("User " + usersOverTwentyRatings.get(i) + " included in final draw");
            finalUsers.add(usersOverTwentyRatings.get(i));
        }

        int entriesDeleted = 0;
        // delete the entires of users not selected for the final draw
        for(int i = data.numInstances()-1; i >= 0; i--) {
            Instance currentInstance = data.instance(i);
            if(finalUsers.contains((int) currentInstance.value(0)) == false) {
                data.delete(i);
                entriesDeleted++;
            }
        }

        System.out.println(entriesDeleted + " entries deleted");

        BufferedWriter writer = new BufferedWriter(new FileWriter("vergo_small.arff"));
        writer.write(data.toString());
        writer.flush();
        writer.close();
    }
}
