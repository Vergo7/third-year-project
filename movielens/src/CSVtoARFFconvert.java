import au.com.bytecode.opencsv.CSVReader;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.CSVSaver;
import weka.core.converters.ConverterUtils;
import weka.filters.unsupervised.attribute.AddID;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.logging.Filter;

/**
 * Created by Varun on 12/11/2014.
 */
public class CSVtoARFFconvert {
    public static void main(String[] args) throws Exception {

/*        Instances data = null;
        try {
            data = ConverterUtils.DataSource.read("u2base.csv");

        } catch (Exception ex) {
            ex.printStackTrace();
        }*/

        CSVtoARFF("u_sortedByUser");
    }

    public static void CSVtoARFF(String csvFilename) throws Exception {
        FastVector atts = new FastVector();
        Instances data;

        for(int i = 0; i < 1682; i++) {
            atts.addElement(new Attribute("item_" + (i+1)));
        }

        data = new Instances("MyRelation", atts, 0);

        CSVReader reader = new CSVReader(new FileReader(csvFilename + ".csv"));

        int userIndex = 1;
        double item_id;
        double rating;
        double[] vals = new double[data.numAttributes()];

        String [] nextLine;
        while ((nextLine = reader.readNext()) != null) {
            // nextLine[] is an array of values from the line

            if(Double.parseDouble(nextLine[0]) == userIndex) {
                item_id = Double.parseDouble(nextLine[1]);
                rating = Double.parseDouble(nextLine[2]);

                vals[(int)item_id - 1] = rating;
            } else {
                data.add(new Instance(1.0, vals));
                userIndex++;
                vals = new double[data.numAttributes()];
                item_id = Double.parseDouble(nextLine[1]);
                rating = Double.parseDouble(nextLine[2]);

                vals[(int)item_id - 1] = rating;
            }
        }
        data.add(new Instance(1.0, vals));  // need this for last user in the file

        weka.filters.unsupervised.attribute.AddID addID = new AddID();
        addID.setIDIndex("last");
        addID.setAttributeName("ID");
        addID.setInputFormat(data);
        data = weka.filters.Filter.useFilter(data, addID);

        BufferedWriter writer = new BufferedWriter(new FileWriter(csvFilename + ".arff"));
        writer.write(data.toString());
        writer.flush();
        writer.close();
    }
}
