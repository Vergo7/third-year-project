import weka.core.Instances;
import weka.core.converters.ConverterUtils;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) throws Exception {
        ThreadKun threadKun = new ThreadKun();
        Thread thread = new Thread(threadKun);
        thread.start();
    }

}

class ThreadKun implements Runnable {
    public void run() {
        System.out.println("My thread is in running state.");
    }
}
